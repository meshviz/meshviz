import sys
from threading import Thread

# Make ^C work again.
import signal
signal.signal(signal.SIGINT, signal.SIG_DFL)

from PyQt5 import QtCore, QtWidgets, QtGui
from PyQt5.QtCore import QThreadPool, QRunnable

from lib.gui.window.main import *
from lib.gui.exception_handler import *

from lib.project import *

class GuiManager():
	def __init__(self):
		app = QtWidgets.QApplication(sys.argv)

		ExceptionHandler.override_exceptions()
		self.window = MainWindow()
		self.window.show_window(WelcomeWindow)

		sys.exit(app.exec_())
