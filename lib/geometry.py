import math

from lib.point import *

class Geometry:
	def angle_between(point1, point2):
		radians = math.atan2(point2.y - point1.y, point2.x - point1.x)
		degrees = radians * 180 / math.pi
		return degrees

	def distance(point1, point2):
		dx = point1.x - point2.x;
		dy = point1.y - point2.y;
		dist = math.sqrt(dx*dx + dy*dy);
		return dist;

	def intersection(start1, end1, start2, end2):
		x1 = start1.x;
		y1 = start1.y;
		x2 = end1.x;
		y2 = end1.y;
		x3 = start2.x;
		y3 = start2.y;
		x4 = end2.x;
		y4 = end2.y;

		px = ( (x1*y2-y1*x2)*(x3-x4)-(x1-x2)*(x3*y4-y3*x4) ) / ( (x1-x2)*(y3-y4)-(y1-y2)*(x3-x4) )
		py = ( (x1*y2-y1*x2)*(y3-y4)-(y1-y2)*(x3*y4-y3*x4) ) / ( (x1-x2)*(y3-y4)-(y1-y2)*(x3-x4) )
		return Point(px, py)

	def normal(start, end):
		dx = end.x - start.x;
		dy = end.y - start.y;
		return (Point(start.x - dy, start.y + dx), Point(start.x + dy, start.y - dx))

	def point_between(point1, point2, ratio=0.5):
		x = point1.x + (point2.x - point1.x)*ratio
		y = point1.y + (point2.y - point1.y)*ratio
		return Point(x, y)
