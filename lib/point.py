from lib.database_interfaces_manager import *

class Point:
	db_manager = DatabaseInterfacesManager()

	def __init__(self, x, y):
		self.x = x
		self.y = y
		self.id = None

	@classmethod
	def from_qt(self, point):
		return Point(point.x(), point.y())

	def __add__(self, other):
		return Point(self.x + other.x, self.y + other.y)

	def __getitem__(self, index):
		if index == 0:
			return self.x;
		elif index == 1:
			return self.y;
		else:
			raise Exception("Unknown index for Point: %s" % index);

	def __sub__(self, other):
		return Point(self.x - other.x, self.y - other.y)

	def to_tuple(self):
		return (self.x, self.y)

	def save(self, filename= "database.db"):
		db_manager = DatabaseInterfacesManager(filename)
		self.id = db_manager.point_db_interface.add_point(self.x, self.y)
		return self

	def __repr__(self):
		return "(%s, %s)" % (self.x, self.y)

	# You shouldn't change the point since that will screw with the database
	# Instead you should update the point positions
	def update(self, point):
		self.x = point.x
		self.y = point.y

	# __eq__ makes objects unhashable in python
	def at_same_point(self, other_point):
		return self.x == other_point.x and self.y == other_point.y

	def saveable_attrs(self):
		return (
				self.x,
				self.y
				)
