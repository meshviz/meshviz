class DatabaseCollection(set):
	def __init__(self):
		super().__init__()
		self.related_objects = {}

	def saved(self):
		saved_objects = set()
		for saveable_object in self:
			if saveable_object.id != None:
				saved_objects.add(saveable_object)
		return saved_objects

	def notsaved(self):
		return self - self.saved()

	def save(self, filename):
		for related_object_collection in self.related_objects.values():
			related_object_collection.save(filename)

		ordered_items = list(self.notsaved())

		saveable_attrs = list(map(lambda item: item.saveable_attrs(), ordered_items))

		saved_ids = self._add_items(saveable_attrs, filename)
		for index, item in enumerate(ordered_items):
			item.id = saved_ids[index]

	def delete_ids(self):
		for related_object_collection in self.related_objects.values():
			related_object_collection.delete_ids()
		for item in self:
			item.id = None
