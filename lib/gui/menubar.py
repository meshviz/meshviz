from PyQt5.QtWidgets import QAction, QMenuBar, QApplication

from lib.gui.window.about import *
from lib.gui.window.add_resistor import *
from lib.gui.window.add_resistor_type import *
from lib.gui.window.mesh_check import *
from lib.gui.window.new_mesh import *
from lib.gui.window.open import *
from lib.gui.window.save import *
from lib.gui.window.welcome import *

class MenuBar():
	def __init__(self, window):
		self.window = window
		self.bar = window.menuBar()

	def action(self, title, shortcut = None, tooltip = None, action = None, checkbox = None):
		item = QAction(title, self.window)

		if checkbox != None:
			item.setCheckable(True)
			item.setChecked(checkbox)
		if shortcut:
			item.setShortcut(shortcut)
		if tooltip:
			item.setStatusTip(tooltip)
		if action:
			item.triggered.connect(action)

		return item

class MainMenuBar(MenuBar):
	def __init__(self, window):
		super().__init__(window)

		menu = self.bar.addMenu("&File")
		menu.addAction(self.action("&New", shortcut = "Ctrl+N", tooltip = "New project.", action = lambda: window.show_window(WelcomeWindow)))
		menu.addAction(self.action("&Open", shortcut = "Ctrl+O", tooltip = "Open a project.", action = lambda: window.show_window(OpenWindow) ))
		menu.addAction(self.action("&Save", shortcut = "Ctrl+S", tooltip = "Save the project.", action = lambda: window.show_window(SaveWindow) ))
		menu.addAction(self.action("&Close", shortcut = "Ctrl+Q", tooltip = "Close the application.", action = window.close))

		menu = self.bar.addMenu("&Mesh")
		menu.addAction(self.action("&New", shortcut = "Ctrl+T", tooltip = "New mesh.", action = lambda: window.show_window(NewMeshWindow) ))
		menu.addAction(self.action("&Add Resistor", shortcut = "Shift+A", tooltip = "Add new resistor to mesh.", action = lambda: window.show_window(AddResistorWindow) ))
		menu.addAction(self.action("&Add Resistor Type", shortcut = "Ctrl+Y", tooltip = "Add new resistor type.", action = lambda: window.show_window(AddResistorTypeWindow) ))
		menu.addAction(self.action("&Delete Elements", shortcut = "Del", tooltip = "Delete the selected element.", action = lambda: window.visualizer.delete_selected()))
		menu.addAction(self.action("&Select all", shortcut = "Ctrl+A", tooltip = "Select all elements.", action = lambda: window.visualizer.select_all()))

		menu = self.bar.addMenu("&Tools")
		menu.addAction(self.action("&Resistance calculator", tooltip = "Calculate the equivalent resistance between two nodes.", action = lambda: window.visualizer.add_resistance_calculator()))
		menu.addAction(self.action("&Validity checker", tooltip = "Check whether the selected mesh is valid.", action = lambda: window.show_window(MeshCheckWindow) ))
		menu.addAction(self.action("&Breakage tool", shortcut = "B", tooltip = "Simulate mesh breakage across all layers.", action = lambda v: window.visualizer.scene.setBreakageMode(v), checkbox = False ))

		menu = self.bar.addMenu("&View");
		menu.addAction(self.action("&Show resistance values", tooltip = "Show resistance values inside resistor boxes.", action = lambda v: window.config('show_resistivity_values', v), checkbox = True))
		menu.addAction(self.action("&Show ghost mesh", tooltip = "Show other meshes in the project as 'ghosts' below the current mesh.", action = lambda v: window.config('show_ghost_mesh', v), checkbox = True))

		menu = self.bar.addMenu("&Help")
		menu.addAction(self.action("&About", tooltip = "Show information about the software.", action = lambda: window.show_window(AboutWindow) ))
