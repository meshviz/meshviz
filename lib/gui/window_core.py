from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import QSize, QThread, Qt
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QMainWindow, QLabel, QGridLayout, QWidget

from time import sleep

class Window(QMainWindow):
	def show(self, close_event = None):
		self.close_event = close_event;
		super().show();

	def close(self, *args):
		super().close();
		if hasattr(self, 'close_event') and self.close_event != None:
			self.close_event(*args);

	def __init__(self, parent=None, size=(340, 160), title="MeshViz", icon='assets/icon.png'):
		super().__init__(parent)

		self.setMinimumSize(QSize(*size))
		self.setWindowTitle(title)
		self.setWindowFlags(Qt.Dialog)

		if (icon):
			self.setWindowIcon(QIcon(icon))

		centralWidget = QWidget(self)
		self.setCentralWidget(centralWidget)

		self.layout = QGridLayout(self)
		centralWidget.setLayout(self.layout)

		self.setWindowFlags(Qt.Dialog);
