import math;

from lib.geometry import *;

class Snapper:
	def add(self, point, value):
		self.points[value] = Point(point.x, point.y);
		self._get_and_create(point.x, point.y).append(value);

	def get(self, point):
		x, y = point.x, point.y;

		values = self._get(x, y);

		if len(values) == 0:
			return None;

		nearest_value = None;
		nearest_value_distance = math.inf;
		for value in values:
			distance = Geometry.distance(point, self.points[value]);
			if (distance < nearest_value_distance):
				nearest_value_distance = distance;
				nearest_value = value;

		return nearest_value;

	def translate_coordinates(self, x, y):
		x -= self.snap_distance / 2;
		y -= self.snap_distance / 2;

		x //= self.snap_distance;
		y //= self.snap_distance;

		return x, y;

	def _get(self, x, y):
		x, y = self.translate_coordinates(x, y);

		if not x in self.grid:
			return [];
		if not y in self.grid[x]:
			return [];

		return self.grid[x][y];

	def _get_and_create(self, x, y):
		x, y = self.translate_coordinates(x, y);

		if not x in self.grid:
			self.grid[x] = {};
		if not y in self.grid[x]:
			self.grid[x][y] = [];

		return self.grid[x][y];

	def remove(self, value):
		if value not in self.points:
			return;

		point = self.points[value];
		self._get(point.x, point.y).remove(value);

		del self.points[value];

	def __init__(self, snap_distance = 40):
		self.snap_distance = snap_distance;
		self.grid = {};
		self.points = {};
