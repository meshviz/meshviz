from PyQt5.QtCore import Qt
from PyQt5.QtGui import QBrush, QPen, QPainterPath, QTransform, QColor
from PyQt5.QtWidgets import QGraphicsItem, QGraphicsItemGroup, QGraphicsEllipseItem, QGraphicsRectItem, QGraphicsPathItem

from lib.point import *
from lib.geometry import *

radius = 3

ghost_color = QColor(222, 222, 222);

class GhostNode(QGraphicsEllipseItem):
	def remove(self):
		self.parent.parent.scene.removeItem(self)

	def update(self):
		self.setPos(self.node.point.x, self.node.point.y)

	def __init__(self, parent, node):
		self.parent = parent;
		self.node = node;

		super().__init__(-radius, -radius, radius*2, radius*2)

		self.setPen(QPen(Qt.NoPen));
		self.setBrush(QBrush(ghost_color));

		self.setZValue(-1);

		self.parent.parent.scene.addItem(self)

		self.update();

rect_width = 40
rect_height = 10

line_length = 10

class GhostResistor(QGraphicsRectItem):
	def remove(self):
		self.parent.parent.scene.removeItem(self)
		self.parent.parent.scene.removeItem(self.resistor_from_wire)
		self.parent.parent.scene.removeItem(self.resistor_to_wire)

	def update(self):
		angle = Geometry.angle_between(self.resistor.from_node.point, self.resistor.to_node.point)

		color = ghost_color;
		if self.resistor.resistor_type.material == 'Broken':
			color = QColor(255, 202, 202);

		self.setBrush(QBrush(color));

		pen = QPen(color);
		pen.setWidth(3);
		self.resistor_from_wire.setPen(pen);

		pen = QPen(color);
		pen.setWidth(3);
		self.resistor_to_wire.setPen(pen);

		self.setPos(self.resistor.point.x, self.resistor.point.y);
		self.setRotation(angle);

		self.render_wire(self.resistor_from_wire, self.resistor.to_node, self.resistor.from_node);
		self.render_wire(self.resistor_to_wire, self.resistor.from_node, self.resistor.to_node);

	def render_wire(self, wire, from_node, to_node):
		angle = Geometry.angle_between(from_node.point, to_node.point)

		# By "port" here I mean the point at which the wires will meet at the node
		from_node_port = Geometry.point_between(from_node.point, to_node.point, ratio=0.1)
		to_node_port = Geometry.point_between(to_node.point, from_node.point, ratio=0.1)

		resistor_wire_path = QPainterPath()

		resistor_wire_path.moveTo(0, 0);
		resistor_wire_path.lineTo(-line_length, 0);

		# Create a transform to draw everything with the correct angles
		wire_transform = QTransform()
		wire_transform.translate(self.resistor.point.x, self.resistor.point.y)
		wire_transform.rotate(angle)
		resistor_wire_path = wire_transform.map(resistor_wire_path);

		normal = Geometry.normal(from_node_port, to_node_port)

		magic_point = Geometry.intersection(
			normal[0],
			normal[1],
			self.resistor.point,
			Point.from_qt(resistor_wire_path.currentPosition())
		)

		resistor_wire_path.lineTo(magic_point.x, magic_point.y);
		resistor_wire_path.lineTo(from_node_port.x, from_node_port.y);
		resistor_wire_path.lineTo(from_node.point.x, from_node.point.y);

		wire.setPath(resistor_wire_path);

	def __init__(self, parent, resistor):
		self.parent = parent;
		self.resistor = resistor;

		super().__init__(-rect_width/2, -rect_height/2, rect_width, rect_height)

		self.setPen(QPen(Qt.NoPen));

		self.resistor_from_wire = QGraphicsPathItem()
		self.resistor_to_wire = QGraphicsPathItem()

		self.parent.parent.scene.addItem(self.resistor_from_wire)
		self.parent.parent.scene.addItem(self.resistor_to_wire)

		self.parent.parent.scene.addItem(self)

		self.update()

		# Display below everything else
		self.resistor_from_wire.setZValue(-5);
		self.resistor_to_wire.setZValue(-5);
		self.setZValue(-5);

class VisualizerGhostMesh(QGraphicsItemGroup):
	def update(self):
		# Additions
		for node in self.mesh.nodes:
			if node not in self.nodes:
				self.nodes[node] = GhostNode(self, node);

		for resistor in self.mesh.resistors:
			if resistor not in self.resistors:
				self.resistors[resistor] = GhostResistor(self, resistor);

		# Deletions and updates
		for node in set(self.nodes):
			if node not in self.mesh.nodes:
				self.nodes[node].remove();
				del self.nodes[node];
				continue;
			self.nodes[node].update();

		for resistor in set(self.resistors):
			if resistor not in self.mesh.resistors:
				self.resistors[resistor].remove();
				del self.resistors[resistor];
				continue;
			self.resistors[resistor].update();

	def remove(self):
		for node in self.nodes:
			self.nodes[node].remove();
		for resistor in self.resistors:
			self.resistors[resistor].remove();

		self.nodes = {};
		self.resistors = {};

	def __init__(self, parent, mesh):
		self.parent = parent
		self.mesh = mesh

		self.nodes = {};
		self.resistors = {};

		super().__init__()

		self.update()
