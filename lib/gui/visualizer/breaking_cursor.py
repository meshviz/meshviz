from PyQt5.QtCore import Qt
from PyQt5.QtGui import QBrush, QPen, QColor
from PyQt5.QtWidgets import QGraphicsItem, QGraphicsItemGroup, QGraphicsEllipseItem

from lib.point import *
from lib.geometry import *

circle_radius = 60

class Circle(QGraphicsEllipseItem):
	def __init__(self, parent, radius):
		super().__init__(-radius, -radius, radius*2, radius*2)

		self.setParentItem(parent);

		self.highlight = False;

	@property
	def highlight(self):
		return self.__highlight;

	@highlight.setter
	def highlight(self, value):
		self.__active = value;
		if value:
			self.setPen(QPen(Qt.red));
			self.setBrush(QBrush(QColor(255, 0, 0, 100)));
		else:
			self.setPen(QPen(Qt.black));
			self.setBrush(QBrush(QColor(0, 0, 0, 0)));

class VisualizerBreakingCursor(QGraphicsItemGroup):
	def remove(self):
		self.parent.removeItem(self.circle);
		self.parent.removeItem(self);

	def activate(self):
		self.circle.highlight = True;
		self.active = True;

	def deactivate(self):
		self.circle.highlight = False;
		self.active = False;

	def __init__(self, parent):
		self.parent = parent;
		self.active = False;
		self.radius = circle_radius;

		super().__init__()

		self.circle = Circle(self, self.radius)

		self.setFlag(QGraphicsItem.ItemSendsGeometryChanges)

		self.addToGroup(self.circle)

		self.setZValue(-10);

		self.parent.addItem(self)
