from PyQt5.QtCore import Qt, QLineF, QMimeData, QPoint, QPointF
from PyQt5.QtGui import QBrush, QTransform, QDrag, QPainterPath, QPen, QColor
from PyQt5.QtWidgets import QGraphicsItem, QGraphicsRectItem, QGraphicsLineItem, QGraphicsSimpleTextItem, QGraphicsEllipseItem, QGraphicsItemGroup, QApplication, QGraphicsPathItem, QMenu, QAction

from lib.geometry import *

from lib.node import *

rect_width = 40
rect_height = 20

line_length = rect_height + 10

line_thickness = 3

class Text():
	def update_text(self, text):
		self.remove_text()
		self.text = QGraphicsSimpleTextItem(text)

		self.text.setPos(-self.text.boundingRect().width()/2, -self.text.boundingRect().height()/2)
		self.text.setZValue(-2);
		self.text.setParentItem(self.parent)

	def remove_text(self):
		if hasattr(self, 'text'):
			self.parent.parent.scene.removeItem(self.text)
			del self.text

	def __init__(self, parent):
		self.parent = parent

class Line():
	def render_wire(self, wire, from_node, to_node):
		angle = Geometry.angle_between(from_node.point, to_node.point)

		# By "port" here I mean the point at which the wires will meet at the node
		from_node_port = Geometry.point_between(from_node.point, to_node.point, ratio=0.1)
		to_node_port = Geometry.point_between(to_node.point, from_node.point, ratio=0.1)

		resistor_wire_path = QPainterPath()

		resistor_wire_path.moveTo(0, 0);
		resistor_wire_path.lineTo(-line_length, 0);

		# Create a transform to draw everything with the correct angles
		wire_transform = QTransform()
		wire_transform.translate(self.resistor.point.x, self.resistor.point.y)
		wire_transform.rotate(angle)
		resistor_wire_path = wire_transform.map(resistor_wire_path);

		normal = Geometry.normal(from_node_port, to_node_port)

		magic_point = Geometry.intersection(
			normal[0],
			normal[1],
			self.resistor.point,
			Point.from_qt(resistor_wire_path.currentPosition())
		)

		resistor_wire_path.lineTo(magic_point.x, magic_point.y);
		resistor_wire_path.lineTo(from_node_port.x, from_node_port.y);
		resistor_wire_path.lineTo(from_node.point.x, from_node.point.y);

		wire.setPath(resistor_wire_path);

	def update(self):
		resistor = self.resistor

		try:
			self.render_wire(self.resistor_from_wire, self.resistor.to_node, self.resistor.from_node);
			self.render_wire(self.resistor_to_wire, self.resistor.from_node, self.resistor.to_node);
		except ZeroDivisionError:
			pass

	def __init__(self, parent):
		self.resistor = parent.resistor
		self.scene = parent.scene

		self.resistor_from_wire = QGraphicsPathItem()
		self.resistor_to_wire = QGraphicsPathItem()

		self.resistor_from_wire.setZValue(-1);
		self.resistor_to_wire.setZValue(-1);

		pen = QPen(Qt.black);
		pen.setWidth(3);
		self.resistor_from_wire.setPen(pen);

		pen = QPen(Qt.black);
		pen.setWidth(3);
		self.resistor_to_wire.setPen(pen);

		self.scene.addItem(self.resistor_from_wire)
		self.scene.addItem(self.resistor_to_wire)

		self.update()

class ColorRect(QGraphicsRectItem):
	def update(self):
		angle = Geometry.angle_between(self.parent.resistor.from_node.point, self.parent.resistor.to_node.point)

		pen = QPen(QColor(self.parent.resistor.resistor_type.color))
		pen.setWidth(line_thickness);
		self.rect.setPen(pen)

		self.setRotation(angle)

		if self.parent.parent.parent.show_resistivity_values:
			self.text.update_text("%.2f" % (self.parent.resistor.resistivity))
		else:
			self.text.remove_text();

	def __init__(self, parent, width, height):
		self.parent = parent

		super().__init__(-width/2, -height/2, width, height)

		self.setPos(parent.resistor.point.x, parent.resistor.point.y)
		self.setBrush(QBrush(Qt.white))
		self.setCursor(Qt.ClosedHandCursor)

		self.rect = QGraphicsRectItem(-width/2, -height/2, width, height)

		# self.rect.setBrush(QBrush(Qt.white))
		self.rect.setCursor(Qt.ClosedHandCursor)

		self.rect.setParentItem(self)

		self.text = Text(self)

		self.update()

class VisualizerResistor(QGraphicsItemGroup):
	def remove(self):
		self.parent.scene.removeItem(self.line.resistor_from_wire)
		self.parent.scene.removeItem(self.line.resistor_to_wire)
		self.parent.scene.removeItem(self);

	def update(self):
		if self.stay_between_nodes:
			self.recenter_resistor()
		self.line.update()
		self.rect.update()

	def grab(self):
		for node in self.resistor.nodes:
			if len(node.resistors) == 1:
				self.parent.nodes[node].setSelected(True)

	def recenter_resistor(self):
		self.stay_between_nodes = True
		self.resistor.point.update(Geometry.point_between(self.resistor.from_node.point, self.resistor.to_node.point))
		self.setPos(self.resistor.point.x, self.resistor.point.y)

	def mouseMoveEvent(self, event):
		super().mouseMoveEvent(event)

		for selected_item in self.parent.selection:
			if isinstance(selected_item, VisualizerResistor):
				# If both nodes are in the selection, leave stay_between_nodes as is.
				for node in selected_item.resistor.nodes:
					if not self.parent.nodes[node] in self.parent.selection:
						selected_item.stay_between_nodes = False
						break;

			selected_item.update();

	def hoverEnterEvent(self, event):
		self.parent.parent.statusBar().showMessage("Resistor: Resistivity: %s Ω, Material: %s, Length: %s" % (self.resistor.resistivity, self.resistor.resistor_type.material, self.resistor.length));
		return super().hoverEnterEvent(event);

	def itemChange(self, change, value):
		if change == QGraphicsItem.ItemPositionChange:
			self.resistor.point.update(Point.from_qt(self.rect.scenePos()))

		if change == QGraphicsItem.ItemSelectedChange:
			# Value is 1 if selected, 0 if not selected
			if value:
				self.parent.selection.add(self);
			else:
				self.parent.selection.remove(self);

		return QGraphicsItem.itemChange(self, change, value);

	def contextMenuEvent(self, event):
		self.setSelected(True);

		menu = QMenu();
		disconnect_action = QAction("Disconnect");
		disconnect_action.triggered.connect(self.parent.disconnect);
		menu.addAction(disconnect_action);

		edit_action = QAction("Edit");
		edit_action.triggered.connect(self.parent.edit);
		menu.addAction(edit_action);

		recenter_action = QAction("Recenter resistor");
		recenter_action.triggered.connect(self.parent.recenter_resistors);
		menu.addAction(recenter_action);

		delete_action = QAction("Delete");
		delete_action.triggered.connect(self.parent.delete_selected);
		menu.addAction(delete_action);

		menu.exec(event.screenPos());

	def mouseDoubleClickEvent(self, event):
		for node in self.resistor.nodes:
			self.parent.nodes[node].setSelected(True)

	def disconnect(self):
		old_from_node = self.resistor.from_node;
		old_to_node = self.resistor.to_node;

		# Create new node points
		self.resistor.from_node = self.parent.mesh.add_node(point = self.resistor.from_node.point.to_tuple())
		self.resistor.to_node = self.parent.mesh.add_node(point = self.resistor.to_node.point.to_tuple())

		# Update the visualization set, removing nodes that are not connected to anything
		if old_from_node.alone:
			self.parent.remove_node(old_from_node);
		if old_to_node.alone:
			self.parent.remove_node(old_to_node);

		self.parent.add_node(self.resistor.from_node, self.resistor.to_node);

	def __init__(self, parent, resistor):
		self.parent = parent
		self.scene = parent.scene
		self.resistor = resistor
		self.parent = parent

		super().__init__()

		self.line = Line(self)
		self.rect = ColorRect(self, rect_width, rect_height)

		self.setFlag(QGraphicsItem.ItemIsSelectable)
		self.setFlag(QGraphicsItem.ItemIsMovable)
		self.setFlag(QGraphicsItem.ItemSendsGeometryChanges)

		self.setAcceptHoverEvents(True)

		self.setPos(resistor.point.x, resistor.point.y)

		self.stay_between_nodes =  resistor.point.at_same_point(Geometry.point_between(resistor.from_node.point, resistor.to_node.point))

		self.addToGroup(self.rect)
		self.scene.addItem(self)
