from PyQt5.QtCore import Qt
from PyQt5.QtGui import QBrush, QPen
from PyQt5.QtWidgets import QGraphicsItem, QGraphicsItemGroup, QGraphicsEllipseItem

from lib.point import *

circle_radius = 10

class Circle(QGraphicsEllipseItem):
	def __init__(self, parent, radius):
		super().__init__(-radius, -radius, radius*2, radius*2)

		self.setParentItem(parent);
		self.setBrush(QBrush(Qt.black));
		self.setCursor(Qt.ClosedHandCursor);

		self.snap = False;

	@property
	def snap(self):
		return self.__snap;

	@snap.setter
	def snap(self, value):
		self.__snap = value;
		if value:
			self.setScale(1.2);
			self.setBrush(QBrush(Qt.yellow));
		else:
			self.setScale(1);
			self.setBrush(QBrush(Qt.black));

class VisualizerResistanceCalculatorNode(QGraphicsItemGroup):
	def remove(self):
		if hasattr(self, 'snap_node'):
			self.snap_node.circle.snap = False;

		self.parent.parent.scene.removeItem(self.circle);
		self.parent.parent.scene.removeItem(self);

	def update(self):
		self.parent.update();

	def release(self):
		# If we have an active snap node, snap this node to that node.
		if hasattr(self, 'snap_node'):
			snap_node = self.snap_node;

			snap_node.circle.snap = False;
			self.circle.snap = False;

			self.setPos(snap_node.scenePos());
			self.point.update(Point.from_qt(snap_node.scenePos()))

			self.update();
			snap_node.update();

		self.parent.update_value();

	def mouseMoveEvent(self, event):
		for selected_item in self.parent.parent.selection:
			selected_item.update();

		super().mouseMoveEvent(event)

	def itemChange(self, change, value):
		if change == QGraphicsItem.ItemPositionChange:
			self.point.update(Point.from_qt(self.scenePos()))

			# Snapping, remove self temporarily
			# self.parent.snapper.remove(self);
			snap_node = self.parent.parent.snapper.get(self.point);
			# self.parent.snapper.add(self.point, self);

			# When snap node changes
			if hasattr(self, 'snap_node') and snap_node != self.snap_node:
				self.snap_node.circle.snap = False;
				self.circle.snap = False;
				del self.snap_node;

			if snap_node:
				self.snap_node = snap_node;
				self.snap_node.circle.snap = True;
				self.circle.snap = True;

		if change == QGraphicsItem.ItemSelectedChange:
			if value:
				self.parent.parent.selection.add(self);
			else:
				self.parent.parent.selection.remove(self);

		return QGraphicsItem.itemChange(self, change, value);

	def __init__(self, parent, point = Point(0, 0)):
		self.parent = parent;
		self.point = point;

		super().__init__()

		self.circle = Circle(self, circle_radius)

		self.setPos(self.point.x, self.point.y);

		self.setFlag(QGraphicsItem.ItemIsSelectable)
		self.setFlag(QGraphicsItem.ItemIsMovable)
		self.setFlag(QGraphicsItem.ItemSendsGeometryChanges)

		self.addToGroup(self.circle)
		self.parent.parent.scene.addItem(self)
