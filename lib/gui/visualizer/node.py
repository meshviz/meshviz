from PyQt5.QtCore import Qt
from PyQt5.QtGui import QBrush, QPen
from PyQt5.QtWidgets import QGraphicsItem, QGraphicsItemGroup, QGraphicsEllipseItem

from lib.point import *
from lib.geometry import *

circle_radius = 6

class Circle(QGraphicsEllipseItem):
	def __init__(self, parent, radius):
		super().__init__(-radius, -radius, radius*2, radius*2)

		self.setParentItem(parent);
		self.setPen(QPen(Qt.NoPen));
		self.setBrush(QBrush(Qt.black));
		self.setCursor(Qt.ClosedHandCursor);

		self.snap = False;

	@property
	def snap(self):
		return self.__snap;

	@snap.setter
	def snap(self, value):
		self.__snap = value;
		if value:
			self.setScale(1.2);
			self.setBrush(QBrush(Qt.yellow));
		else:
			self.setScale(1);
			self.setBrush(QBrush(Qt.black));

class VisualizerNode(QGraphicsItemGroup):
	def remove(self):
		self.parent.scene.removeItem(self.circle);
		self.parent.scene.removeItem(self);

	def update(self):
		for resistor in self.node.resistors:
			self.parent.resistors[resistor].update();

	def release(self):
		# If we have an active snap node, snap this node to that node.
		if hasattr(self, 'snap_node'):
			for resistor in set(self.node.resistors):
				if (resistor.from_node == self.node):
					resistor.from_node = self.snap_node.node;
				else:
					resistor.to_node = self.snap_node.node;

				# Remove resistors that became illegal because of the snap.
				if resistor.from_node == resistor.to_node:
					self.parent.remove_resistor(resistor);

			# Remove this node if there are no resistors connected anymore.
			if self.node.alone:
				self.parent.remove_node(self.node);

			if hasattr(self.snap_node, 'snap_node'):
				del self.snap_node.snap_node;

			self.snap_node.circle.snap = False;
			self.snap_node.update();

	def mouseMoveEvent(self, event):
		for selected_item in self.parent.selection:
			selected_item.update();

		return super().mouseMoveEvent(event)

	def itemChange(self, change, value):
		if change == QGraphicsItem.ItemPositionChange:
			self.node.point.update(Point.from_qt(self.scenePos()))

			# Snapping, remove self temporarily
			self.parent.snapper.remove(self);
			snap_node = self.parent.snapper.get(self.node.point);
			self.parent.snapper.add(self.node.point, self);

			# When snap node changes
			if hasattr(self, 'snap_node') and snap_node != self.snap_node:
				self.snap_node.circle.snap = False;
				self.circle.snap = False;
				del self.snap_node;

			if snap_node:
				self.snap_node = snap_node;
				self.snap_node.circle.snap = True;
				self.circle.snap = True;

		if change == QGraphicsItem.ItemSelectedChange:
			# Value is 1 if selected, 0 if not selected
			if value:
				self.parent.selection.add(self);
			else:
				self.parent.selection.remove(self);

		return QGraphicsItem.itemChange(self, change, value);

	def __init__(self, parent, node):
		self.node = node;
		self.parent = parent;

		super().__init__()

		self.circle = Circle(self, circle_radius)

		self.setPos(node.point.x, node.point.y);

		self.setFlag(QGraphicsItem.ItemIsSelectable)
		self.setFlag(QGraphicsItem.ItemIsMovable)
		self.setFlag(QGraphicsItem.ItemSendsGeometryChanges)

		self.addToGroup(self.circle)
		self.parent.scene.addItem(self)
