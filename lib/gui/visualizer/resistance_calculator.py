from PyQt5.QtCore import Qt
from PyQt5.QtGui import QBrush, QPen, QTextOption, QPainterPath, QFont
from PyQt5.QtWidgets import QGraphicsItem, QGraphicsRectItem, QGraphicsItemGroup, QGraphicsEllipseItem, QGraphicsTextItem, QGraphicsPathItem

from lib.point import *
from lib.resistance_calculator import *

from lib.gui.visualizer.resistance_calculator_node import *

class Text(QGraphicsTextItem):
	def __init__(self, parent, text):
		super().__init__(text);

		self.setParentItem(parent);

		self.setFont(QFont("Sans", 18))
		self.setPos(-self.boundingRect().width()/2, -self.boundingRect().height()/2);

class Rect(QGraphicsRectItem):
	def __init__(self, parent, width, height):
		self.parent = parent

		super().__init__(-width/2, -height/2, width, height)

		self.setPos(parent.point.x, parent.point.y)

		self.text = Text(self, "??? Ω");

		self.setBrush(QBrush(Qt.white))
		self.setCursor(Qt.ClosedHandCursor)

class Line():
	def remove(self):
		self.parent.parent.scene.removeItem(self.a_wire);
		self.parent.parent.scene.removeItem(self.b_wire);

	def render_wire(self, wire, from_point, to_point):
		wire_path = QPainterPath()

		wire_path.moveTo(from_point.x, from_point.y);
		wire_path.lineTo(to_point.x, to_point.y);

		wire.setPath(wire_path);

	def update(self):
		try:
			self.render_wire(self.a_wire, self.parent.point, self.parent.a_node.point);
			self.render_wire(self.b_wire, self.parent.point, self.parent.b_node.point);
		except ZeroDivisionError:
			pass

	def __init__(self, parent):
		self.parent = parent;

		self.a_wire = QGraphicsPathItem()
		self.b_wire = QGraphicsPathItem()

		pen = QPen(Qt.black);
		pen.setWidth(5);
		self.a_wire.setPen(pen);

		pen = QPen(Qt.black);
		pen.setWidth(5);
		self.b_wire.setPen(pen);

		self.parent.parent.scene.addItem(self.a_wire)
		self.parent.parent.scene.addItem(self.b_wire)

		self.update();

width = 180
height = 80

class VisualizerResistanceCalculator(QGraphicsItemGroup):
	def remove(self):
		self.a_node.remove();
		self.b_node.remove();
		self.line.remove();

		self.parent.scene.removeItem(self);

	def update(self):
		self.line.update();

	def update_value(self):
		if hasattr(self.a_node, 'snap_node') and hasattr(self.b_node, 'snap_node'):
			self.update_text("%s Ω" % '...');
			if not self.parent.update_resistance(self, self.a_node.snap_node.node, self.b_node.snap_node.node, lambda value: self.update_text("%s Ω" % round(value, 2))):
				self.update_text("%s Ω" % '???');
		else:
			self.update_text("%s Ω" % '???');

	def update_text(self, text):
		self.parent.scene.removeItem(self.rect.text);
		self.rect.text = Text(self, text);

	def mouseMoveEvent(self, event):
		for selected_item in self.parent.selection:
			selected_item.update();

		super().mouseMoveEvent(event)

	def itemChange(self, change, value):
		if change == QGraphicsItem.ItemPositionChange:
			self.point = Point.from_qt(self.scenePos())

		if change == QGraphicsItem.ItemSelectedChange:
			if value:
				self.parent.selection.add(self);
			else:
				self.parent.selection.remove(self);

		return QGraphicsItem.itemChange(self, change, value);

	def __init__(self, parent, point = Point(0, 0)):
		self.parent = parent;
		self.point = point;

		super().__init__()

		self.setFlag(QGraphicsItem.ItemIsSelectable)
		self.setFlag(QGraphicsItem.ItemIsMovable)
		self.setFlag(QGraphicsItem.ItemSendsGeometryChanges)

		self.rect = Rect(self, width, height);

		self.a_node = VisualizerResistanceCalculatorNode(self, point = point + Point(width/2, -height/2));
		self.b_node = VisualizerResistanceCalculatorNode(self, point = point + Point(width/2, height/2));

		self.a = None;
		self.b = None;

		self.line = Line(self);

		self.addToGroup(self.rect)
		self.parent.scene.addItem(self)
