from PyQt5.QtCore import Qt, QThread, pyqtSignal
from PyQt5.QtWidgets import QGraphicsView, QGraphicsScene

from lib.mesh import *
from lib.mesh_checker import *
from lib.node import *

from lib.point import *
from lib.geometry import *

from lib.gui.visualizer.ghost_mesh import *
from lib.gui.visualizer.node import *
from lib.gui.visualizer.resistance_calculator import *
from lib.gui.visualizer.resistor import *

from lib.gui.widgets.default_scene import *

from lib.gui.snapper import *

from lib.gui.window.edit_resistor import *

class ResistanceThread(QThread):
	valuesig = pyqtSignal(float);

	def get_mesh(self, mesh):
		self.mesh = mesh;

	def get_a(self, a):
		self.a = a;

	def get_b(self, b):
		self.b = b;

	def run(self):
		self.valuesig.emit(ResistanceCalculator.equivalent_resistance((self.a, self.b), self.mesh));

class MeshVisualizer(QGraphicsView):
	meshsig = pyqtSignal(Mesh);
	nodeasig = pyqtSignal(Node);
	nodebsig = pyqtSignal(Node);

	threads = set()

	def add_node(self, *nodes):
		for node in nodes:
			self.nodes[node] = VisualizerNode(self, node);
			self.snapper.add(node.point, self.nodes[node]);

	def add_resistor(self, *resistors):
		for resistor in resistors:
			self.resistors[resistor] = VisualizerResistor(self, resistor);

	def add_resistance_calculator(self):
		self.resistance_calculators.add(VisualizerResistanceCalculator(self));

	def break_resistors_in_area(self, point, radius):
		# Get a rectangle of scene elements.
		rect = QRectF(point.x - radius, point.y - radius, point.x + radius, point.y + radius);
		items = self.scene.items(rect);

		# Loop through the rectangle, check that they are actually inside the radius.
		updated = False
		for item in items:
			if Geometry.distance(Point.from_qt(item.scenePos()), point) < radius:
				# Only work on resistors or ghost resistors.
				if type(item) == VisualizerResistor or type(item) == GhostResistor:
					item.resistor.break_resistor();
					item.update();
					updated = True

		if updated:
			self.mesh_updated();

	def update(self):
		for resistor in self.resistors:
			self.resistors[resistor].update();

	def update_ghost(self, mesh):
		# Displaying ourselves as ghost would be outright silly.
		if mesh == self.mesh:
			return;

		if mesh in self.ghosts:
			self.ghosts[mesh].update()
		else:
			self.ghosts[mesh] = VisualizerGhostMesh(self, mesh);

	def remove_ghost(self, mesh):
		if mesh in self.ghosts:
			self.ghosts[mesh].remove()
			del self.ghosts[mesh];

	def mesh_updated(self):
		for calculator in self.resistance_calculators:
			calculator.update_value();

	def update(self):
		for resistor in self.resistors:
			self.resistors[resistor].update();

	# TODO: Move this back to calculator class
	def update_resistance(self, calculator, a, b, value_callback):
		if a not in self.mesh.nodes or b not in self.mesh.nodes:
			return False

		thread = ResistanceThread()
		self.threads.add(thread)

		self.meshsig.connect(thread.get_mesh);
		self.meshsig.emit(self.mesh);

		self.nodeasig.connect(thread.get_a);
		self.nodeasig.emit(a);

		self.nodebsig.connect(thread.get_b);
		self.nodebsig.emit(b);

		thread.valuesig.connect(value_callback);

		thread.start();
		return True

	def select_all(self):
		for resistor in self.resistors:
			self.resistors[resistor].setSelected(True);
		for node in self.nodes:
			self.nodes[node].setSelected(True);

	def remove_node(self, *nodes):
		for node in nodes:
			if not node in self.nodes:
				continue

			# Remove from selection.
			viz_node = self.nodes[node];
			viz_node.setSelected(False);
			if viz_node in self.selection:
				self.selection.remove(viz_node)

			# Remove from snapping.
			self.snapper.remove(self.nodes[node]);

			# Remove all resistors connected to this node.
			for resistor in set(node.resistors):
				self.remove_resistor(resistor);

			# Remove from mesh.
			self.mesh.remove_node(node);

			# Remove visual node.
			if node in self.nodes:
				self.nodes[node].remove();
				del self.nodes[node];

	def remove_resistor(self, *resistors):
		for resistor in resistors:
			if not resistor in self.resistors:
				continue

			# Remove from selection.
			viz_resistor = self.resistors[resistor];
			if viz_resistor in self.selection:
				self.selection.remove(viz_resistor)

			# Remove visual resistor.
			viz_resistor.remove();
			del self.resistors[resistor];

			# Remove resistor from mesh.
			nodes = set(resistor.nodes);
			self.mesh.remove_resistor(resistor);

			# Remove dangling nodes.
			for node in nodes:
				if node.alone:
					self.remove_node(node);

	def remove_resistance_calculator(self, calculator):
		self.resistance_calculators.remove(calculator);
		self.selection.remove(calculator);
		calculator.remove();

	def delete_selected(self):
		for item in set(self.selection):
			if type(item) == VisualizerResistor:
				self.remove_resistor(item.resistor);
			if type(item) == VisualizerNode:
				self.remove_node(item.node);
			if type(item) == VisualizerResistanceCalculator:
				self.remove_resistance_calculator(item);

		self.mesh_updated();

	def wheelEvent(self, event):
		super().wheelEvent(event);
		self.parent.sync_scenes(self);

	def disconnect(self):
		for item in set(self.selection):
			if hasattr(item, 'disconnect'):
				item.disconnect()

		self.mesh_updated();

	def edit(self):
		resistors = [];
		for item in set(self.selection):
			if type(item) == VisualizerResistor:
				resistors.append(item)

		self.editwindow = EditResistorWindow(self.parent, resistors)
		self.editwindow.show(self.mesh_updated)
		self.editwindow.activateWindow()

	def recenter_resistors(self):
		for item in set(self.selection):
			if hasattr(item, 'recenter_resistor'):
				item.recenter_resistor()
				item.update()

	def show_mesh(self, mesh):
		self.mesh = mesh

		self.scene = QGraphicsScene(self)

		for resistor in self.mesh.resistors:
			self.add_resistor(resistor);
		for node in self.mesh.nodes:
			self.add_node(node);

		self.setScene(self.scene)

	def mousePressEvent(self, event):
		super().mousePressEvent(event);
		for item in set(self.selection):
			if hasattr(item, 'grab'):
				item.grab();

	def mouseReleaseEvent(self, event):
		super().mouseReleaseEvent(event);

		snapped = False
		for item in set(self.selection):
			if hasattr(item, 'release'):
				if hasattr(item, 'snap_node'):
					snapped = True
				item.release();

		if snapped:
			self.mesh_updated()

	def __init__(self, parent, mesh):
		self.parent = parent;
		self.mesh = mesh

		self.snapper = Snapper();

		self.nodes = {}
		self.resistors = {}
		self.selection = set()
		self.resistance_calculators = set()
		self.ghosts = {}

		super().__init__();

		self.scene = DefaultScene(self)

		self.setScene(self.scene)

		self.setDragMode(QGraphicsView.RubberBandDrag)
		self.setStyleSheet("background: transparent");

		self.scrollAmount = (0, 0);

		for resistor in self.mesh.resistors:
			self.add_resistor(resistor);
		for node in self.mesh.nodes:
			self.add_node(node);
