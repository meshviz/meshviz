from PyQt5.QtWidgets import QGraphicsScene
from PyQt5.QtCore import QRectF

from lib.gui.visualizer.breaking_cursor import *

class DefaultScene(QGraphicsScene):
	def setBreakageMode(self, value):
		if value:
			if not self.breakage:
				self.cursor = VisualizerBreakingCursor(self);
				self.parent.setCursor(Qt.CrossCursor);
		else:
			if self.breakage:
				self.cursor.remove();
				del self.cursor;
				self.parent.setCursor(Qt.ArrowCursor);

	def mouseDoubleClickEvent(self, event):
		if self.breakage:
			# Prevent propagation to other events.
			event.accept();
		else:
			return super().mouseDoubleClickEvent(event);

	def mouseMoveEvent(self, event):
		if self.breakage:
			event.accept();
			self.cursor.setPos(event.scenePos());
			if self.cursor.active:
				self.parent.break_resistors_in_area(Point.from_qt(event.scenePos()), self.cursor.radius);
		else:
			return super().mouseMoveEvent(event);

	def mousePressEvent(self, event):
		if self.breakage:
			event.accept();
			self.cursor.activate();
			if self.cursor.active:
				self.parent.break_resistors_in_area(Point.from_qt(event.scenePos()), self.cursor.radius);
		else:
			return super().mousePressEvent(event);

	def mouseReleaseEvent(self, event):
		if self.breakage:
			event.accept();
			self.cursor.deactivate();
		else:
			return super().mouseReleaseEvent(event);

	@property
	def breakage(self):
		return hasattr(self, 'cursor');

	def __init__(self, parent):
		self.parent = parent

		super().__init__(parent);
