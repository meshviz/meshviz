from PyQt5.QtWidgets import QComboBox, QGridLayout, QWidget, QPushButton;

from lib.project import *

from lib.gui.window.add_resistor_type import *

class ResistorTypeList(QWidget):
	def __init__(self, parent, main_window):
		super().__init__(parent);

		self.choices = {};

		self.parent = main_window;
		self.layout = QGridLayout(self);

		self.combobox = ResistorBox(self);
		self.layout.addWidget(self.combobox, 0, 0, 1, 20)

		button = QPushButton("+")
		button.clicked.connect(self.add_resistor_type)
		self.layout.addWidget(button, 0, 20, 1, 1)

	def add_resistor_type(self):
		self.add_resistor_type_window = AddResistorTypeWindow(self)
		self.add_resistor_type_window.show(lambda resistor_type: self.set_choice(resistor_type))

	def set_choice(self, resistor_type):
		self.update()
		for index, other_type in self.choices.items():
			if other_type == resistor_type:
				self.combobox.setCurrentIndex(index)

	def choice(self):
		index = self.combobox.currentIndex()
		if (index < 0):
			return None;
		return self.choices[index];

	def update(self):
		self.combobox.clear()

		if len(Project.resistor_types) == 0:
			self.parent.show_window(AddResistorTypeWindow)
			return;

		for resistor_type in list(Project.resistor_types):
			if resistor_type.material == "Broken":
				continue
			self.choices[self.combobox.count()] = resistor_type
			self.combobox.addItem(resistor_type.material)

class ResistorBox(QComboBox):
	def __init__(self, parent=None):
		self.parent = parent
		super().__init__(parent);

	def showPopup(self):
		self.parent.update();
		super().showPopup();
