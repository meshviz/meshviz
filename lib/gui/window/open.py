from lib.project import *
from lib.mesh import *
from lib.database_interfaces.project_database_interface import *

from lib.gui.window_core import *
from PyQt5.QtWidgets import QFileDialog, QMessageBox

class OpenWindow(Window):
	def __init__(self, parent):
		super().__init__(title="Save")
		self.parent = parent

	def show(self):
		filename, _ = QFileDialog.getOpenFileName(self, "Open MeshViz Project", None, "MeshViz Files (*.meshviz)")

		if filename:
			Project.filename = filename

			if (ProjectDatabaseInterface.load_project(filename) == False):
				QMessageBox.critical(None, "Error", "Unfortunately, no meshes have been found", QMessageBox.Cancel)
				return

			for mesh in Project.meshes.values():
				self.parent.add_tab(mesh);

		self.close()
