from PyQt5.QtWidgets import QHBoxLayout, QWidget, QRadioButton, QButtonGroup, QSizePolicy, QPushButton, QLineEdit, QMenu, QAction, QComboBox
from PyQt5.QtGui import QIntValidator

from lib.predefmesh import *
from lib.project import *
from lib.resistor_type import *

from lib.gui.widgets.resistor_type_list import *

from lib.gui.window_core import *

class NewMeshWindow(Window):
	def __init__(self, parent):
		super().__init__(title="New Mesh")

		self.parent = parent
		radio = QWidget()

		layout = QHBoxLayout()
		radio.setLayout(layout)

		button = QPushButton("Empty Mesh")
		button.clicked.connect(self.empty_mesh)
		self.layout.addWidget(button)

		numberLabel = QLabel()
		numberLabel.setText("Layout:")
		self.layout.addWidget(numberLabel)

		self.group = QButtonGroup(radio)

		self.grid = QRadioButton("Grid")
		self.grid.setChecked(True)
		self.group.addButton(self.grid)
		layout.addWidget(self.grid)

		self.series = QRadioButton("Series")
		self.group.addButton(self.series)
		layout.addWidget(self.series)

		self.parallel = QRadioButton("Parallel")
		self.group.addButton(self.parallel)
		layout.addWidget(self.parallel)

		self.layout.addWidget(radio)

		onlyInt = QIntValidator()

		numberLabel = QLabel()
		numberLabel.setText("Width:")
		self.layout.addWidget(numberLabel)
		self.horizontal = QLineEdit()
		self.horizontal.setText("5")
		self.horizontal.setValidator(onlyInt)
		self.layout.addWidget(self.horizontal)

		numberLabel = QLabel()
		numberLabel.setText("Height:")
		self.layout.addWidget(numberLabel)
		self.vertical = QLineEdit()
		self.vertical.setText("5")
		self.vertical.setValidator(onlyInt)
		self.layout.addWidget(self.vertical)

		self.layout.addWidget(QLabel("Mesh material:"))

		self.type = ResistorTypeList(self, self.parent)
		self.layout.addWidget(self.type)

		button = QPushButton("Generate")
		button.clicked.connect(self.generate)
		self.layout.addWidget(button)

		button = QPushButton("Cancel")
		button.clicked.connect(self.close)
		self.layout.addWidget(button)

	def show(self):
		self.type.update()
		super().show()

	def generate(self):
		if (self.vertical.text() == "" or self.horizontal.text() == "" or self.type.choice() == None):
			return;

		vertical = int(self.vertical.text())
		horizontal = int(self.horizontal.text())
		resistor_type = self.type.choice()

		predef = Predefmesh()
		choice = self.group.checkedButton()

		print("Generating a %sx%s %s mesh..." % (horizontal, vertical, resistor_type.material))

		if choice == self.series:
			mesh = predef.series_stacked(vertical, horizontal, resistor_type)
		elif choice == self.parallel:
			mesh = predef.parallel_joined(vertical, horizontal, resistor_type)
		else:
			mesh = predef.grid(vertical, horizontal, resistor_type)

		Project.add_mesh(mesh)
		self.parent.add_tab(mesh)

		self.close()

	def empty_mesh(self):
		mesh = Mesh()
		Project.add_mesh(mesh)
		self.parent.add_tab(mesh)
		self.close()
