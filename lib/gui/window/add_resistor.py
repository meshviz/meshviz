from PyQt5.QtWidgets import *

from lib.gui.window_core import *
from lib.gui.widgets.resistor_type_list import *

from lib.resistor import *
from lib.node import *
from lib.resistor_type import *

from lib.point import *
from lib.project import *

class AddResistorWindow(Window):
	def __init__(self, parent):
		super().__init__(title="Add Resistor")
		self.parent = parent

		self.layout.addWidget(QLabel("Length (m):"))
		self.length = QLineEdit()
		self.length.setText("1")
		self.length.setValidator(QDoubleValidator())
		self.layout.addWidget(self.length)

		self.layout.addWidget(QLabel("Material:"))

		self.type = ResistorTypeList(self, self.parent)
		self.layout.addWidget(self.type)

		button = QPushButton("Add")
		button.clicked.connect(self.add)
		self.layout.addWidget(button)
		button = QPushButton("Cancel")
		button.clicked.connect(self.close)
		self.layout.addWidget(button)

	def add(self):
		if (self.length.text() == "" or self.type.choice() == None):
			return;

		resistor_type = self.type.choice()
		length = float(self.length.text())

		mesh = self.parent.visualizer.mesh;

		scene_pos = self.parent.visualizer.mapToScene(self.parent.visualizer.rect().center())
		scene_pos = Point.from_qt(scene_pos)

		node1 = Node(point = scene_pos + Point(44, 44), mesh_id = Project.current_id)
		node2 = Node(point = scene_pos - Point(44, 44), mesh_id = Project.current_id)

		new_resistor = mesh.add_resistor(resistor_type, length, node1, node2, point = scene_pos)

		# Add visual elements
		for node in new_resistor.nodes:
			self.parent.visualizer.add_node(node)
		self.parent.visualizer.add_resistor(new_resistor)

		self.close()

	def show(self):
		self.type.update()
		super().show()
