from lib.gui.window_core import *
from lib.gui.window.new_mesh import *
from lib.gui.window.open import *
from lib.project import *
from lib.database_interfaces.project_database_interface import *

from PyQt5.QtWidgets import QAction, QMessageBox, QApplication, QGridLayout, QPushButton, QSizePolicy, QLabel
from PyQt5.QtCore import Qt
from PyQt5 import QtGui

class TopBar():
	def __init__(self):
		self.widget = QWidget()

		topbarlayout = QGridLayout(self.widget)
		label = QLabel(self.widget)
		pixmap = QtGui.QPixmap('assets/meshviz.jpeg')
		label.setPixmap(pixmap.scaledToHeight(64))
		label.setAlignment(Qt.AlignRight)
		topbarlayout.addWidget(label, 0, 1, 0, 1)

		title = QLabel("Welcome to MeshViz!", self.widget)
		title.setFont(QtGui.QFont('SansSerif', 30))
		topbarlayout.addWidget(title, 0, 0, 0, 1)

		self.widget.setLayout(topbarlayout)

class BottomBar():
	def __init__(self):
		self.widget = QWidget()
		self.widget.setStyleSheet('background-color: orange')

class CenterContent():
	def __init__(self):
		self.widget = QWidget()
		self.widget.setStyleSheet('background-color: #ddd')

class NewProjectButton():
	def __init__(self, parent):
		self.parent = parent
		self.widget = QPushButton("New Project")
		self.widget.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
		self.widget.setStyleSheet('margin: 22px')
		self.widget.clicked.connect(self.parent.new_project)

class OpenProjectButton():
	def __init__(self, parent):
		self.parent = parent
		self.widget = QPushButton("Open Project")
		self.widget.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
		self.widget.setStyleSheet('margin: 22px')
		self.widget.clicked.connect(self.parent.open_project)

class WelcomeWindow(Window):
	def __init__(self, parent):
		super().__init__(title="Welcome to MeshViz!", size=(800, 400))

		self.parent = parent

		self.layout.addWidget(TopBar().widget, 0, 0, 1, 0)
		self.layout.addWidget(BottomBar().widget, 9, 0, 1, 0)

		centerlayout = QGridLayout(self)
		centerlayout.addWidget(NewProjectButton(self).widget, 0, 1, 0, 1)
		centerlayout.addWidget(OpenProjectButton(self).widget, 0, 2, 0, 1)

		center = CenterContent()
		center.widget.setLayout(centerlayout)

		self.layout.addWidget(center.widget, 1, 0, 8, 0)

	def new_project(self):
		for index in list(Project.meshes.keys()):
			self.parent.close_tab(index)
		ProjectDatabaseInterface.new_project()
		self.close()
		self.parent.show()
		self.parent.show_window(NewMeshWindow)

	def open_project(self):
		self.close()
		self.parent.show()
		self.parent.show_window(OpenWindow)
