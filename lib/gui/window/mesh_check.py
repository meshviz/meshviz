from lib.gui.window_core import *
from lib.mesh_checker import *
from lib.project import *

from PyQt5.QtWidgets import QMessageBox, QHBoxLayout, QWidget, QRadioButton, QButtonGroup, QSizePolicy, QPushButton, QLineEdit, QMenu, QAction, QComboBox
from PyQt5.QtGui import QIntValidator

class MeshCheckWindow(Window):
	def __init__(self, parent):
		super().__init__(title="Validity Check")
		self.parent = parent

		self.check_button = QPushButton("Check")
		self.check_button.clicked.connect(self.check)
		self.layout.addWidget(self.check_button)

		self.cancel_button = QPushButton("Cancel")
		self.cancel_button.clicked.connect(self.close)
		self.layout.addWidget(self.cancel_button)

	def check(self):
		mesh = self.parent.visualizer.mesh
		if mesh != None:
			check = MeshChecker.check(mesh)
			self.post_check(check)

	def post_check(self, decision):
		if decision == True:
			buttonReply = QMessageBox.information(self, 'Result', "The Mesh is Valid", QMessageBox.Ok)
		else:
			buttonReply = QMessageBox.critical(self, 'Result', "The Mesh is Invalid", QMessageBox.Ok)
		if buttonReply == QMessageBox.Ok:
			self.close()
