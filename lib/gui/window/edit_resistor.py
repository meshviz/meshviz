from PyQt5.QtWidgets import QHBoxLayout, QWidget, QRadioButton, QButtonGroup, QSizePolicy, QPushButton, QLineEdit, QMenu, QAction, QComboBox
from PyQt5.QtGui import QDoubleValidator

from lib.gui.window_core import *
from lib.project import *

from lib.gui.widgets.resistor_type_list import *

import random

class EditResistorWindow(Window):
	def __init__(self, parent, resistors):
		super().__init__(title="Edit")
		self.parent = parent
		self.resistors = resistors
		radio = QWidget()

		layout = QHBoxLayout()
		radio.setLayout(layout)

		button = QPushButton("Break")
		button.clicked.connect(self.breakage)
		self.layout.addWidget(button)

		self.layout.addWidget(QLabel("Length (m):"))
		self.length = QLineEdit()
		self.length.setValidator(QDoubleValidator())
		self.layout.addWidget(self.length)

		self.layout.addWidget(QLabel("Material:"))

		self.type = ResistorTypeList(self, parent)

		# Get one of the types we have in the selection, and set that as default value.
		random_resistor = random.choice(resistors).resistor
		self.type.set_choice(random_resistor.resistor_type)
		# Same for the length.
		self.length.setText(str(random_resistor.length))

		self.layout.addWidget(self.type)

		button = QPushButton("Change")
		button.clicked.connect(self.change)
		self.layout.addWidget(button)

		button = QPushButton("Cancel")
		button.clicked.connect(lambda: self.close())
		self.layout.addWidget(button)

	def change(self):
		if (self.type.choice() == None) or (self.length.text() == None):
			return;

		resistor_type = self.type.choice()
		length = float(self.length.text())

		for viz_resistor in self.resistors:
			viz_resistor.resistor.resistor_type = resistor_type
			viz_resistor.resistor.length = length
			viz_resistor.update()

		self.close()

	def breakage(self):
		for viz_resistor in self.resistors:
			viz_resistor.resistor.break_resistor()
			viz_resistor.update()
		self.close()
