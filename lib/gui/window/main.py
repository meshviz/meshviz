from lib.project import *
from lib.gui.window_core import *
from lib.gui.menubar import *
from lib.mesh import *
from lib.user_error import *

from PyQt5.QtCore import Qt, QRectF
from PyQt5.QtWidgets import QAction, QMessageBox, QApplication, QInputDialog, QFileDialog

from lib.gui.widgets.mesh_visualizer import *

class TopBar():
	def __init__(self):
		self.widget = QWidget()
		self.widget.setStyleSheet('background-color: orange')

class SideBar(QtWidgets.QTabWidget):
	def __init__(self):
		QtWidgets.QTabWidget.__init__(self)
		stylesheet = "QTabBar::tab:selected {background: pink;}"
		self.setStyleSheet(stylesheet)

class MainWindow(Window):
	def __init__(self):
		self.visualizers = {};
		self.windows = set();

		super().__init__(size=(1200, 700))

		self.setWindowFlags(Qt.Widget);

		self.statusBar().showMessage('Ready')

		self.show_resistivity_values = True
		self.show_ghost_mesh = True

		self.menu_bar = MainMenuBar(self)
		self.layout.addWidget(TopBar().widget, 0, 0, 1, 0)

		self.sidebar = SideBar()
		self.sidebar.setTabsClosable(True)
		self.sidebar.setMovable(True)
		self.sidebar.tabCloseRequested.connect(self.close_tab)
		self.sidebar.currentChanged.connect(self.switch_tab)
		self.layout.addWidget(self.sidebar, 10, 0, 10, 1)

	def show_window(self, window):
		wind = window(self);
		self.windows.add(wind);
		wind.show();

	def config(self, item, value):
		setattr(self, item, value);

	@property
	def show_resistivity_values(self):
		return self.__show_resistivity_values;
	
	@show_resistivity_values.setter
	def show_resistivity_values(self, value):
		self.__show_resistivity_values = value;
		if hasattr(self, 'visualizer'):
			self.visualizer.update();

	@property
	def show_ghost_mesh(self):
		return self.__show_ghost_mesh;
	
	@show_ghost_mesh.setter
	def show_ghost_mesh(self, value):
		self.__show_ghost_mesh = value;
		if hasattr(self, 'visualizer'):
			self.update_ghosts();

	def sync_scenes(self, visualizer = None):
		if visualizer == None:
			visualizer = QtWidgets.QTabWidget.currentWidget(self.sidebar)

		boundingRect = visualizer.scene.itemsBoundingRect()
		scroll_x = visualizer.horizontalScrollBar().value()
		scroll_y = visualizer.verticalScrollBar().value()

		for visualizer in self.visualizers.keys():
			visualizer.scene.setSceneRect(boundingRect)
			visualizer.horizontalScrollBar().setValue(scroll_x)
			visualizer.verticalScrollBar().setValue(scroll_y)

	@property
	def visualizer(self):
		visualizer = QtWidgets.QTabWidget.currentWidget(self.sidebar)
		if visualizer == None:
			raise UserError("No mesh selected")

		Project.current_mesh = self.visualizers[visualizer];
		return visualizer

	def add_tab(self, mesh):
		current_visualizer = QtWidgets.QTabWidget.currentWidget(self.sidebar)
		if current_visualizer != None:
			current_visualizer.update_ghost(mesh)

		visualizer = MeshVisualizer(self, mesh)
		index = Project.current_id
		self.visualizers[visualizer] = index
		self.sidebar.addTab(visualizer, "Layer " + str(index))

	def close_tab(self, index):
		removed_visualizer = self.sidebar.widget(index)

		Project.remove_mesh(removed_visualizer.mesh);
		del self.visualizers[removed_visualizer];

		for visualizer in self.visualizers:
			visualizer.remove_ghost(removed_visualizer.mesh);

		removed_visualizer.deleteLater()

	def update_ghosts(self):
		current_visualizer = QtWidgets.QTabWidget.currentWidget(self.sidebar)
		if current_visualizer != None:
			for visualizer in self.visualizers.keys():
				if self.show_ghost_mesh:
					current_visualizer.update_ghost(visualizer.mesh)
				else:
					current_visualizer.remove_ghost(visualizer.mesh)
			current_visualizer.update()

	def switch_tab(self):
		self.update_ghosts()

	def closeEvent(self, event):
		if QMessageBox.question(self, 'Message', 'Are you sure you want to quit?', QMessageBox.Yes, QMessageBox.No) == QMessageBox.Yes:
			event.accept()
			QApplication.instance().closeAllWindows()
		else:
			event.ignore()
