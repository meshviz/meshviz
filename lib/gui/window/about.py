from lib.gui.window_core import *

class AboutWindow(Window):
	def __init__(self, parent):
		super().__init__(title="About MeshViz")
		message = "<h2>About</h2>"
		message += "<p>MeshViz is a complex simulator for a resistive network and its elements.<br> It is part of a larger collaboration with teams of engineers at the Robert Gordon University and the University of Aberdeen.<br> For instructions on how to use it, please refer to the user manual.</p>"
		message += "<br><br><h2>Authors</h2>"
		message += "<p>Valentina Daga</p>"
		message += "<p>Mikko Ilmonen</p>"
		message += "<p>Ioana Lucaci</p>"
		message += "<p>Keeyan Nejad</p>"
		message += "<p>Emilia Podstawka</p>"
		self.title = QLabel(message, self)
		self.title.setAlignment(QtCore.Qt.AlignCenter)

		self.layout.addWidget(self.title, 0, 0)
