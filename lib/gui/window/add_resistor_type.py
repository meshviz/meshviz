from PyQt5.QtWidgets import QLineEdit, QColorDialog, QPushButton
from PyQt5.QtGui import QDoubleValidator

from lib.gui.window_core import *

from lib.project import *

from lib.resistor_type import *

class AddResistorTypeWindow(Window):
	def __init__(self, parent):
		super().__init__(title="Add Resistor Type")

		label = QLabel()
		label.setText("Material name:")
		self.layout.addWidget(label)

		self.material = QLineEdit()
		self.material.setPlaceholderText("Copper")
		self.layout.addWidget(self.material)

		label = QLabel()
		label.setText("Resistance value (Ω * m):")
		self.layout.addWidget(label)

		self.resistivity = QLineEdit()
		self.resistivity.setPlaceholderText("20")
		self.resistivity.setValidator(QDoubleValidator())
		self.layout.addWidget(self.resistivity)

		label = QLabel()
		label.setText("Label colour:")
		self.layout.addWidget(label)

		self.color = QColorDialog()
		self.color.setOption(QColorDialog.NoButtons);
		self.layout.addWidget(self.color)

		button = QPushButton("Add")
		button.clicked.connect(self.add)
		self.layout.addWidget(button)
		button = QPushButton("Cancel")
		button.clicked.connect(self.close)
		self.layout.addWidget(button)

		self.parent = parent

	def add(self):
		if (self.material.text() == "" or self.resistivity.text() == ""):
			return;

		material = self.material.text()
		resistivity = float(self.resistivity.text())
		color = self.color.currentColor().name()

		resistor_type = ResistorType(material, resistivity, color);

		Project.resistor_types.add(resistor_type);

		self.close(resistor_type);
