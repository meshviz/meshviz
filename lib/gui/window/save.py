import os

from lib.project import *
from lib.gui.window_core import *
from PyQt5.QtWidgets import QFileDialog

class SaveWindow(Window):
	def __init__(self, parent):
		super().__init__(title="Save")
		self.parent = parent

	def show(self):
		filename = Project.filename

		if not filename:
			filename, _ = QFileDialog.getSaveFileName(self, "Save MeshViz Project", None, "MeshViz Files (*.meshviz)")
			if not filename:
				return self.close()

		if not filename.endswith('.meshviz'):
			filename += '.meshviz'

		# This is a hack. What would be better is to define update methods for all classes and fix the project load to load IDs.
		# However simply deleting the file before saving it will prevent save duplication errors
		if os.path.isfile(filename):
			os.remove(filename)

		Project.filename = filename
		self._delete_all_ids()
		for mesh in Project.meshes.values():
			mesh.save(filename)

		self.parent.statusBar().showMessage("Saved to %s" % filename)

	def _delete_all_ids(self):
		for mesh in Project.meshes.values():
			mesh.id = None
			mesh.resistors.delete_ids()
			mesh.nodes.delete_ids()
