from PyQt5 import QtCore
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QMessageBox
from lib.user_error import *
import sys
import traceback

class ExceptionHandler(QtCore.QObject):
	errorSignal = QtCore.pyqtSignal()

	def __init__(self):
		super(ExceptionHandler, self).__init__()

	def handler(self, exctype, value, error_traceback):
		self.errorSignal.emit()
		sys._excepthook(exctype, value, error_traceback)

		if exctype is UserError:
			self.handle_user_error(value)
		else:
			self.handle_system_error(exctype, value, error_traceback)

	def handle_user_error(self, value):
		errorbox = QMessageBox()
		errorbox.setText(str(value))
		errorbox.exec_()

	def handle_system_error(self, exctype, value, error_traceback):
		errorbox = QMessageBox()
		errorbox.setText("Unfortunately, Something went wrong.\nPlease submit a bug report at https://gitlab.com/meshviz/meshviz/issues/new")
		errorbox.setStyleSheet("QLabel{min-width: 500px;}");
		errorbox.setDetailedText(''.join(traceback.format_tb(error_traceback)) + repr(value))
		errorbox.exec_()

	def override_exceptions():
		exceptionHandler = ExceptionHandler()
		sys._excepthook = sys.excepthook
		sys.excepthook = exceptionHandler.handler
