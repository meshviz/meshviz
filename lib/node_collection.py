from lib.database_interfaces_manager import *
from lib.database_collection import *
from lib.point_collection import *

class NodeCollection(DatabaseCollection):
	def __init__(self):
		super().__init__()
		self.related_objects["points"] = PointCollection()

	def save(self, mesh_id, filename):
		for item in self:
			item.mesh_id = mesh_id

		super().save(filename)

	def add(self, node):
		super().add(node)
		self.related_objects["points"].add(node.point)

	def _add_items(self, saveable_attrs, filename):
		db = DatabaseInterfacesManager(filename).node_db_interface
		return db.add_nodes(saveable_attrs)
