from lib.database_interfaces.mesh_database_interface import *
from lib.database_interfaces.resistor_database_interface import *
from lib.database_interfaces.resistor_type_database_interface import *
from lib.database_interfaces.node_database_interface import *
from lib.database_interfaces.point_database_interface import *
from lib.database_interfaces.whole_database_interface import *
from lib.database_interfaces.open_database_interface import *

class DatabaseInterfacesManager:
	def __init__(self, filename = "database.db"):

		self.mesh_db_interface = MeshDatabaseInterface(filename)
		self.resistor_db_interface = ResistorDatabaseInterface(filename)
		self.resistor_type_db_interface = ResistorTypeDatabaseInterface(filename)
		self.node_db_interface = NodeDatabaseInterface(filename)
		self.point_db_interface = PointDatabaseInterface(filename)
		self.whole_db_interface = WholeDatabaseInterface(filename)
		self.open_db_interface = OpenDatabaseInterface(filename)
