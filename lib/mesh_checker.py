from lib.mesh import *

class MeshChecker:

	def check(mesh, nodes_to_check = False):
		if not nodes_to_check:
			nodes_to_check = set(mesh.nodes)

		#BFS from: https://www.geeksforgeeks.org/breadth-first-search-or-bfs-for-a-graph/
		visited = {}
		queue = []
		node_dict = {}
		#takes all resistors and 'connects' their to-node with their from-node
		#I use sets to ensure no repetition happens
		for resistor in mesh.resistors:
			if not(resistor.from_node in node_dict.keys()):
				node_dict[resistor.from_node] = set([resistor.to_node])
			if not(resistor.to_node in node_dict.keys()):
				node_dict[resistor.to_node] = set([resistor.from_node])
			node_dict[resistor.to_node].add(resistor.from_node)
			node_dict[resistor.from_node].add(resistor.to_node)

		node_list = list(node_dict.keys())
		for node in node_list:
			visited[node] = False

		#BFS Algorithm:
		start_node = nodes_to_check.pop()
		queue.append(start_node)
		visited[start_node] = True
		result = []
		while queue:
			s = queue.pop()
			result.append(s)
			for i in node_dict[s]:
				if visited[i] == False:
					queue.append(i)
					visited[i] = True
		check = True
		ends = 0
		#Check whether the mesh is valid or not: all nodes must be traversed and only the start and end node can have < 2 resistors connected to them
		for node in nodes_to_check:
			if node not in result:
				return False
		for node in result:
			if (len(node_dict[node]) < 2):
				ends +=1
			if ends >= 3:
				check = False
		return check
