from lib.database_interfaces_manager import *

class ResistorType:
	def __init__(self, material, resistivity, color="#FFAABB"):
		self.material = material
		self.resistivity = resistivity
		self.color = color
		self.id = None

	def save(self, filename = "database.db"):
		db_manager = DatabaseInterfacesManager(filename)
		if self.id == None:
			self.id = db_manager.resistor_type_db_interface.add_resistor_type(self.material, self.resistivity, self.color)
		return self

	def saveable_attrs(self):
		return (
				self.material,
				self.resistivity,
				self.color,
				)
