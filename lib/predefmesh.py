from lib.database_interfaces_manager import *
from lib.mesh import *
from lib.geometry import *
from lib.node import *

class Predefmesh:
	def __init__(self):
		self.nodes = {}

	def series_stacked(self, num_of_wires, num_of_resistors_on_wire, resistor_type):
		self.mesh = Mesh()
		length = 1
		num_of_wires += 1
		first_node_point = (((num_of_wires - 1)* 100)/2, 0)
		last_node_point = (((num_of_wires - 1)* 100)/2, num_of_resistors_on_wire * 100)
		first_node = self.get_node(0, point=first_node_point)
		last_node = self.get_node(-1, point=last_node_point)
		for wire in range(num_of_wires):
			from_node = None
			to_node = None
			for resistor in range(num_of_resistors_on_wire):
				if resistor == 0:
					from_node = self.get_node(0)
				else:
					from_node_point = (wire * 100, resistor * 100)
					from_node = self.get_node((wire,resistor), point=from_node_point)
				if resistor == num_of_resistors_on_wire - 1:
					to_node = self.get_node(-1)
				else:
					to_node_point = (wire * 100, (resistor + 1) * 100)
					to_node = self.get_node((wire,resistor+1), point=to_node_point)

				resistor_point = Geometry.point_between(from_node.point, to_node.point).to_tuple()

				self.mesh.add_resistor(resistor_type, 1, from_node, to_node, point=resistor_point)
		return self.mesh

	def parallel_joined(self, num_of_clusters, num_of_resistors_in_cluster, resistor_type):
		self.mesh = Mesh()

		for i in range(num_of_clusters):
			for j in range(num_of_resistors_in_cluster):
				from_node_point = (i * 100, j * num_of_resistors_in_cluster * 100 + (num_of_resistors_in_cluster * 100)/2)
				to_node_point = (i * 100 + 100, j * num_of_resistors_in_cluster * 100 + (num_of_resistors_in_cluster * 100)/2)

				from_node = self.get_node(i, point=from_node_point)
				to_node = self.get_node(i+1, point=to_node_point)

				resistor_point = (i * 100 + 50, j * 100 + 50)

				self.mesh.add_resistor(resistor_type, 1, from_node, to_node, point=resistor_point)

		return self.mesh

	def grid(self, num_of_vertical_resistors, num_of_horizontal_resistors, resistor_type):
		self.mesh = Mesh()

		number_of_nodes_per_row = num_of_horizontal_resistors + 1
		number_of_nodes_per_column = num_of_vertical_resistors + 1

		for row in range(num_of_vertical_resistors + 1):
			for column in range(num_of_horizontal_resistors):
				from_node_point = (row * 100, column * 100)
				to_node_point = (row * 100, (column + 1) * 100)
				resistor_point = (row * 100, column * 100 + 50)

				from_node = self.get_node(row * number_of_nodes_per_row + column, point=from_node_point)
				to_node = self.get_node(row * number_of_nodes_per_row + column + 1, point=to_node_point)
				self.mesh.add_resistor(resistor_type, 1, from_node, to_node, point=resistor_point)

		for column in range(num_of_horizontal_resistors + 1):
			for row in range(num_of_vertical_resistors):
				resistor_point = (row * 100 + 50, column * 100)

				from_node = self.get_node(row * number_of_nodes_per_row + column)
				to_node = self.get_node((row + 1)* number_of_nodes_per_row + column)
				self.mesh.add_resistor(resistor_type, 1, from_node, to_node, point=resistor_point)

		return self.mesh

	def get_node(self, node_id, point = (0, 0)):
		if node_id in self.nodes:
			return self.nodes[node_id]
		self.nodes[node_id] = self.mesh.add_node(point = point)
		return self.nodes[node_id]
