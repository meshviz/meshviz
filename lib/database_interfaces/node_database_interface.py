import sqlite3

class NodeDatabaseInterface:
	def __init__(self, filename):
		connection = sqlite3.connect(filename)
		self.connection = connection
		self.cursor = connection.cursor()

	def add_node(self, mesh_id, point_id):
		return self.add_nodes([(mesh_id, point_id)])[0]

	def add_nodes(self, nodes):
		if len(nodes) == 0:
			return []
		self.cursor.executemany('''
			INSERT INTO nodes (mesh_id, point_id)
			VALUES (?, ?)
			''', nodes)
		self.connection.commit()
		last_id = self.get_last_id()
		return list(range(last_id - len(nodes) + 1, last_id + 1))

	def get_last_id(self):
		self.cursor.execute('SELECT max(id) FROM nodes')
		return self.cursor.fetchone()[0]
