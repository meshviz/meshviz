import sqlite3
class OpenDatabaseInterface:
	def __init__(self, filename):
		connection = sqlite3.connect(filename)
		self.connection = connection
		self.cursor = connection.cursor()


	def load_meshes(self):
		self.cursor.execute('SELECT * FROM meshes')
		meshes = self.cursor.fetchall()
		return meshes

	def load_resistors(self,mesh_id):
		self.cursor.execute('''
			SELECT * FROM resistors WHERE mesh_id=?
			''', (mesh_id,))
		resistors = self.cursor.fetchall()
		return resistors

	def load_resistor_types(self):
		self.cursor.execute('SELECT id, material, resistivity, color FROM resistor_types')
		resistor_types = self.cursor.fetchall()
		return resistor_types

	def load_points(self):
		self.cursor.execute('SELECT * FROM points')
		points = self.cursor.fetchall()
		return points

	def load_nodes(self, mesh_id):
		self.cursor.execute('''SELECT * FROM nodes WHERE mesh_id=?''', (mesh_id,))
		nodes = self.cursor.fetchall()
		return nodes
