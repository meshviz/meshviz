import math

from lib.project import *
from lib.database_interfaces_manager import *
from lib.resistor_type import *
from lib.mesh import *

class ProjectDatabaseInterface():
	def new_project():
		Project.meshes = {}
		Project.current_id = 0
		Project.resistor_types = ResistorTypeCollection()
		Project.resistor_types.add(ResistorType("Gold", 15, "#FFFC60"))
		Project.resistor_types.add(ResistorType("Silver", 30, "#A09A92"))
		Project.resistor_types.add(ResistorType("Copper", 20, "#684711"))
		Project.resistor_types.add(ResistorType("Broken", math.inf, "#FF0000"))

	def load_project(filename):
		db_manager = DatabaseInterfacesManager(filename) #pass a filename
		mesh_ids = db_manager.open_db_interface.load_meshes() #this loads only mesh ids so we can see how many meshes are saved in file
		resistor_types = db_manager.open_db_interface.load_resistor_types() #this can be taken straight up from db cause its not connected to meshes
		points = db_manager.open_db_interface.load_points()
		point_dict = {}
		for point in points:
			#this ensures all points are stored to be later used for nodes and resistors
			point_dict[point[0]] = (point[1], point[2])
		for resistor_type in resistor_types:
			#this ensures all resistor types will be saved in project and later used for resistors
			resistor_type_instance = ResistorType(resistor_type[1],resistor_type[2], resistor_type[3])
			Project.resistor_types.add(resistor_type_instance)
			Project.loaded_resistor_types.update({resistor_type[0]:resistor_type_instance})

		if len(mesh_ids) == 0:
			return False
		for item in mesh_ids:
			i = item[0]
			mesh = Mesh()
			#couldn't use range since meshes start from 1 in db, not 0
			resistors = db_manager.open_db_interface.load_resistors(i)
			nodes = db_manager.open_db_interface.load_nodes(i)
			#must create a node dictionary to later be used by resistors, based on their id
			node_dict = {}

			for node in nodes:
				#adds all nodes connected to said mesh to the node_dict and finds the points from the point dictionary
				point_of_node = point_dict[node[2]]
				node_dict[node[0]] = mesh.add_node(point_of_node)

			for resistor in resistors:
				#similar to the nodes, takes the point from the dictionary, the resistor type from the dictionary and the to-node and from-node
				resistor_point = point_dict[resistor[5]]
				from_node, to_node = node_dict[resistor[3]], node_dict[resistor[4]]
				mesh.add_resistor(Project.loaded_resistor_types[resistor[2]],resistor[6], from_node, to_node, resistor_point)

			Project.add_mesh(mesh)
		return True
