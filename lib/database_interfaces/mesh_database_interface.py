import sqlite3
class MeshDatabaseInterface:
	def __init__(self, filename):
		connection = sqlite3.connect(filename)
		self.connection = connection
		self.cursor = connection.cursor()

		self.create_tables()

	def create_tables(self):
		self.cursor.execute('''CREATE TABLE IF NOT EXISTS points (
			id INTEGER PRIMARY KEY,
			x REAL,
			y REAL
			)''')
		self.cursor.execute('''CREATE TABLE IF NOT EXISTS meshes (
			id INTEGER PRIMARY KEY
			)''')
		self.cursor.execute('''CREATE TABLE IF NOT EXISTS resistor_types (
			id INTEGER PRIMARY KEY,
			material TEXT,
			resistivity REAL,
			color TEXT
			)''')
		self.cursor.execute('''CREATE TABLE IF NOT EXISTS nodes (
			id INTEGER PRIMARY KEY,
			mesh_id INTEGER,
			point_id INTEGER,
			FOREIGN KEY(point_id) REFERENCES point(id)
			)''')
		self.cursor.execute('''CREATE TABLE IF NOT EXISTS resistors (
			id INTEGER PRIMARY KEY,
			mesh_id INTEGER NOT NULL,
			resistor_type_id INTEGER NOT NULL,
			from_node_id INTEGER NOT NULL,
			to_node_id INTEGER NOT NULL,
			point_id INTEGER,
			length REAL NULL,
			FOREIGN KEY(mesh_id) REFERENCES mesh(id),
			FOREIGN KEY(resistor_type_id) REFERENCES resistor_types(id)
			FOREIGN KEY(point_id) REFERENCES point(id)
			)''')
		self.connection.commit()

	def add_mesh(self):
		self.cursor.execute('INSERT INTO meshes DEFAULT VALUES')
		self.connection.commit()
		return self.cursor.lastrowid
