import sqlite3

class ResistorTypeDatabaseInterface:
	def __init__(self, filename):
		connection = sqlite3.connect(filename)
		self.connection = connection
		self.cursor = connection.cursor()

	def add_resistor_type(self, material, resistivity, color):
		return self.add_resistor_types([(material, resistivity, color)])[0]

	def add_resistor_types(self, resistor_types):
		if len(resistor_types) == 0:
			return []

		self.cursor.executemany('''
			INSERT INTO resistor_types (material, resistivity, color)
			VALUES (?, ?, ?)
			''', resistor_types)

		self.connection.commit()

		last_id = self.get_last_id()
		return list(range(last_id - len(resistor_types) + 1, last_id + 1))

	def get_last_id(self):
		self.cursor.execute('SELECT max(id) FROM resistor_types')
		return self.cursor.fetchone()[0]
