import sqlite3
class WholeDatabaseInterface:
	def __init__(self, filename):
		connection = sqlite3.connect(filename)
		self.connection = connection
		self.cursor = connection.cursor()

	def clean_whole_database(self):
		self.cursor.execute("DELETE FROM meshes")
		self.cursor.execute("DELETE FROM nodes")
		self.cursor.execute("DELETE FROM points")
		self.cursor.execute("DELETE FROM resistors")
		self.cursor.execute("DELETE FROM resistor_types")
		self.connection.commit()
		
	def return_resistortypes(self):
		materials = [types[0] for types in self.cursor.execute("SELECT material FROM resistor_types")]
		return materials
