import sqlite3

class PointDatabaseInterface:
	def __init__(self, filename):
		connection = sqlite3.connect(filename)
		self.connection = connection
		self.cursor = connection.cursor()

	def add_point(self, x, y):
		return self.add_points([(x, y)])[0]

	def add_points(self, points):
		if len(points) == 0:
			return []

		self.cursor.executemany('''
			INSERT INTO points (x, y)
			VALUES (?, ?)
			''', points)
		self.connection.commit()

		last_id = self.get_last_id()
		return list(range(last_id - len(points) + 1, last_id + 1))

	def get_last_id(self):
		self.cursor.execute('SELECT max(id) FROM points')
		return self.cursor.fetchone()[0]
