import sqlite3

class ResistorDatabaseInterface:
	def __init__(self, filename):
		connection = sqlite3.connect(filename)
		self.connection = connection
		self.cursor = connection.cursor()

	def add_resistor(self, mesh_id, resistor_type_id, length, from_node_id, to_node_id, point_id):
		return self.add_resistors([(mesh_id, resistor_type_id, length, from_node_id, to_node_id, point_id)])[0]

	def add_resistors(self, resistors):
		if len(resistors) == 0:
			return []
		self.cursor.executemany('''
			INSERT INTO resistors (mesh_id, resistor_type_id, length, from_node_id, to_node_id, point_id)
			VALUES (?, ?, ?, ?, ?, ?)
			''', resistors)
		self.connection.commit()
		last_id = self.get_last_id()
		return list(range(last_id - len(resistors) + 1, last_id + 1))

	def get_last_id(self):
		self.cursor.execute('SELECT max(id) FROM resistors')
		return self.cursor.fetchone()[0]
