from lib.resistor import *
from lib.node import *
from lib.resistor_type import *

import math

class ResistanceCalculator:
	def equivalent_resistance(terminal_nodes, original_mesh):
		mesh, node_mapping = original_mesh.clone();

		if len(set(terminal_nodes)) == 1:
			return 0

		for resistor in set(mesh.resistors):
			if resistor.resistivity == math.inf:
				mesh.remove_resistor(resistor)

		if len(mesh.resistors) == 0:
			return math.inf

		new_terminal_nodes = [ node_mapping[node] for node in terminal_nodes ];

		while(len(mesh.resistors) != 1):
			ResistanceCalculator.do_single_simplification(new_terminal_nodes, mesh)

		final_resistor = mesh.resistors.pop()
		if final_resistor.nodes == set(new_terminal_nodes):
			return final_resistor.resistivity
		else:
			return math.inf

	def do_single_simplification(terminal_nodes, mesh):
		# This doesn't actually loop over every node. At most it will be 3 since if a node is not terminal, a simplification can happen
		for node in mesh.nodes:
			if ResistanceCalculator.simplify_series_or_parallel(terminal_nodes, node, mesh):
				return True

			if node in terminal_nodes:
				continue

			ResistanceCalculator.star_mesh_transform(node, mesh)
			return True

	def simplify_series_or_parallel(terminal_nodes, node, mesh):
		for resistor in node.resistors:
			resistors_between_nodes = ResistanceCalculator.resistors_between_nodes(resistor.from_node, resistor.to_node, mesh)
			if len(resistors_between_nodes) > 1:
				equivalent_resistance = ResistanceCalculator.parallel_resistance(resistors_between_nodes)

				ResistanceCalculator.remove_resistors(resistors_between_nodes, mesh)
				mesh.add_resistor(ResistorType(None, equivalent_resistance), 1, resistor.from_node, resistor.to_node)
				return True

			resistor_in_series = ResistanceCalculator.resistor_in_series_with(resistor, terminal_nodes, mesh)
			if (resistor_in_series):
				equivalent_resistance = ResistanceCalculator.series_resistance([resistor, resistor_in_series])

				from_node = (resistor.nodes - resistor_in_series.nodes).pop()
				to_node = (resistor_in_series.nodes - resistor.nodes).pop()

				ResistanceCalculator.remove_resistors([resistor, resistor_in_series], mesh)
				mesh.add_resistor(ResistorType(None, equivalent_resistance), 1, from_node, to_node)
				return True

	def resistor_in_series_with(resistor, terminal_nodes, mesh):
		for node in resistor.nodes:
			resistors_with_common_node = set(node.resistors)
			resistors_with_common_node.remove(resistor)
			if (len(resistors_with_common_node) == 1):
				other_resistor = resistors_with_common_node.pop()
				common_node = other_resistor.nodes.intersection(resistor.nodes).pop()
				if (common_node in terminal_nodes):
					continue
				return other_resistor

	def resistors_between_nodes(from_node, to_node, mesh):
		intersection_of_node_resistors = set(from_node.resistors).intersection(to_node.resistors)
		return intersection_of_node_resistors

	def series_resistance(resistors):
		value = sum(map(lambda x: x.resistivity, resistors))
		return value

	def parallel_resistance(resistors):
		value_denominator = sum(map(lambda x: 1/x.resistivity, resistors))
		value = 1/value_denominator
		return value

	def remove_resistors(entries_to_remove, mesh):
		for resistor in entries_to_remove:
			mesh.remove_resistor(resistor)

	# Resistance between any node a and b is Ra * Ra * Σ1/r
	# So just replace all common_node resistors with direct links from each node to each other
	def star_mesh_transform(common_node, mesh):
		resistors_to_transform = set(common_node.resistors)

		sum_of_one_div_r = sum(map(lambda x: 1/x.resistivity, resistors_to_transform))

		remaining_resistors = set(resistors_to_transform)
		for resistor_a in resistors_to_transform:
			remaining_resistors.remove(resistor_a)
			for resistor_b in remaining_resistors:
				node_a = (resistor_a.nodes - set([common_node])).pop()
				node_b = (resistor_b.nodes - set([common_node])).pop()
				resistivity = resistor_a.resistivity * resistor_b.resistivity * sum_of_one_div_r
				mesh.add_resistor(ResistorType(None, resistivity), 1, node_a, node_b)
		ResistanceCalculator.remove_resistors(resistors_to_transform, mesh)
