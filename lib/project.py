class Project:
	filename = None
	meshes = {}
	resistor_types = set()
	current_mesh = None
	current_id = 0
	loaded_resistor_types = {}

	def add_mesh(mesh):
		Project.meshes[Project.current_id] = mesh
		Project.current_id += 1

	def remove_mesh(mesh):
		for mesh_id, other_mesh in Project.meshes.items():
			if mesh == other_mesh:
				del Project.meshes[mesh_id];
				break;
