from lib.project import *
from lib.database_interfaces_manager import *
from lib.database_collection import *

class ResistorTypeCollection(DatabaseCollection):
	def _add_items(self, saveable_attrs, filename):
		db = DatabaseInterfacesManager(filename).resistor_type_db_interface
		return db.add_resistor_types(saveable_attrs)

	@property
	def broken(self):
		for resistor_type in self:
			if resistor_type.material == "Broken":
				return resistor_type
		raise Exception("Broken resistor does not exist")
