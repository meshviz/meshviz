import math

from lib.database_interfaces_manager import *
from lib.point import *
from lib.project import *

from lib.resistor_type import *

class Resistor:
	def __init__(self, resistor_type, length, from_node, to_node, mesh_id = 0, point = (0,0)):
		self.mesh_id = mesh_id
		self.resistor_type = resistor_type
		self.from_node = from_node
		self.to_node = to_node
		self.point = Point(point[0], point[1])
		self.length = length
		self.id = None

	def break_resistor(self):
		self.resistor_type = Project.resistor_types.broken

	def save(self,filename = "database.db"):
		db_manager = DatabaseInterfacesManager(filename)
		self.resistor_type.save(filename)

		for node in self.nodes:
			node.mesh_id = self.mesh_id
			node.save(filename)

		self.point.save(filename)
		if self.id == None:
			self.id = db_manager.resistor_db_interface.add_resistor(
					self.mesh_id,
					self.resistor_type.id,
					self.length,
					self.from_node.id,
					self.to_node.id,
					self.point.id
					)
		return self

	@property
	def from_node(self):
		return self.__from_node;

	@from_node.setter
	def from_node(self, node):
		# Detach from previous node if possible.
		if hasattr(self, 'from_node'):
			self.__from_node.detach(self);
		node.attach(self);
		self.__from_node = node;

	@property
	def to_node(self):
		return self.__to_node;

	@to_node.setter
	def to_node(self, node):
		# Detach from previous node if possible.
		if hasattr(self, 'to_node'):
			self.__to_node.detach(self);
		node.attach(self);
		self.__to_node = node;

	@property
	def resistivity(self):
		return self.resistor_type.resistivity * self.length

	@property
	def nodes(self):
		return set([self.from_node, self.to_node])

	def __repr__(self):
		return (" %s--[%s]--%s " % (self.from_node.id, self.resistivity, self.to_node.id));

	def saveable_attrs(self):
		return (
				self.mesh_id,
				self.resistor_type.id,
				self.length,
				self.from_node.id,
				self.to_node.id,
				self.point.id
				)
