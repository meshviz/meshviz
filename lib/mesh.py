from lib.database_interfaces_manager import *
from lib.node import *
from lib.node_collection import *
from lib.resistor import *
from lib.resistor_collection import *
from lib.clean import *

class Mesh:
	def __init__(self):
		self.resistors = ResistorCollection()
		self.nodes = NodeCollection()
		self.id = None

	def clone(self):
		new_mesh = Mesh()

		node_mapping = {};

		def get_node(node):
			if not node in node_mapping:
				node_mapping[node] = Node()
			return node_mapping[node]

		for resistor in self.resistors:
			new_mesh.add_resistor(resistor.resistor_type, resistor.length, get_node(resistor.from_node), get_node(resistor.to_node))

		return new_mesh, node_mapping

	def save(self,filename = "database.db"):
		db_manager = DatabaseInterfacesManager(filename)
		if self.id == None:
			self.id = db_manager.mesh_db_interface.add_mesh()
		self.nodes.save(self.id, filename)
		self.resistors.save(self.id, filename)
		return self

	def delete(self): #this wipes all resistors from the resistor set
		if self.id != None:
			for resistor in self.resistors: #inside IF since there can't be any resistors to delete unless the mesh was saved
					resistor.delete()

	def add_resistor(self, resistor_type, length, from_node, to_node, point=(0,0)):
		resistor = Resistor(resistor_type, length, from_node, to_node, self.id, point=point)
		self.resistors.add(resistor)

		from_node.attach(resistor)
		to_node.attach(resistor)

		self.nodes.add(from_node)
		self.nodes.add(to_node)
		return resistor

	def add_node(self, point = (0, 0)):
		node = Node(point = point, mesh_id = self.id)
		self.nodes.add(node)
		return node

	def remove_node(self, node):
		if node in self.nodes:
			self.nodes.remove(node)

	def remove_resistor(self, resistor):
		for node in resistor.nodes:
			node.resistors.remove(resistor)
			if node.alone:
				self.remove_node(node)
		self.resistors.remove(resistor)

	def __repr__(self):
		return "%s || %s" % (self.resistors, self.nodes)
