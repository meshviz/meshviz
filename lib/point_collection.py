from lib.database_interfaces_manager import *
from lib.database_collection import *

class PointCollection(DatabaseCollection):
	def _add_items(self, saveable_attrs, filename):
		db = DatabaseInterfacesManager(filename).point_db_interface
		return db.add_points(saveable_attrs)
