from lib.point import *
from lib.database_interfaces_manager import *

class Node:
	def __init__(self, point = (0, 0), mesh_id = 0):
		self.id = None
		self.resistors = set()
		self.mesh_id = mesh_id
		self.point = Point(point[0], point[1])

	def attach(self, resistor):
		self.resistors.add(resistor);

	def detach(self, resistor):
		self.resistors.remove(resistor);

	@property
	def alone(self):
		return len(self.resistors) == 0;

	def save(self, filename= "database.db"):
		# Do not save if there are no resistors connected to this node.
		if self.alone:
			return;

		db_manager = DatabaseInterfacesManager(filename)
		self.point.save(filename)
		if self.id == None:
			self.id = db_manager.node_db_interface.add_node(self.mesh_id, self.point.id)
		return self

	def saveable_attrs(self):
		return (
				self.mesh_id,
				self.point.id
				)
