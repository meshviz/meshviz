from lib.database_interfaces_manager import *
from lib.resistor_type_collection import *
from lib.point_collection import *
from lib.database_collection import *

class ResistorCollection(DatabaseCollection):
	def __init__(self):
		super().__init__()
		self.related_objects["resistor_types"] = ResistorTypeCollection()
		self.related_objects["points"] = PointCollection()

	def save(self, mesh_id, filename):
		for resistor in self:
			resistor.mesh_id = mesh_id
			self.related_objects["resistor_types"].add(resistor.resistor_type)

		super().save(filename)

	def add(self, resistor):
		super().add(resistor)
		self.related_objects["points"].add(resistor.point)

	def _add_items(self, saveable_attrs, filename):
		db = DatabaseInterfacesManager(filename).resistor_db_interface
		return db.add_resistors(saveable_attrs)

