from lib.predefmesh import *
from lib.resistor_type import *
from lib.resistance_calculator import *

def test_4x4_mesh():
	generator = Predefmesh()
	mesh = generator.grid(4, 4, ResistorType("copper", 100))

	node_a = next(iter(mesh.nodes))
	node_b = next(iter(mesh.nodes), 2)

	ResistanceCalculator.equivalent_resistance((node_a, node_b), mesh)

def test_8x8_mesh():
	generator = Predefmesh()
	mesh = generator.grid(8, 8, ResistorType("copper", 100))

	node_a = next(iter(mesh.nodes))
	node_b = next(iter(mesh.nodes), 2)

	ResistanceCalculator.equivalent_resistance((node_a, node_b), mesh)

# def test_16x16_mesh():
#		generator = Predefmesh()
#		mesh = generator.grid(16, 16, ResistorType("copper", 100))
# 
#		node_a = next(iter(mesh.nodes))
#		node_b = next(iter(mesh.nodes), 2)
# 
#		ResistanceCalculator.equivalent_resistance((node_a, node_b), mesh)

# def test_32x32_mesh():
#		generator = Predefmesh()
#		mesh = generator.grid(32, 32, ResistorType("copper", 100))
# 
#		node_a = next(iter(mesh.nodes))
#		node_b = next(iter(mesh.nodes), 2)
# 
#		ResistanceCalculator.equivalent_resistance((node_a, node_b), mesh)
