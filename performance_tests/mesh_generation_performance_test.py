from lib.predefmesh import *
from lib.resistor_type import *

def test_1x1_mesh():
	generator = Predefmesh()
	generator.grid(1, 1, ResistorType("copper", 100))

def test_2x2_mesh():
	generator = Predefmesh()
	generator.grid(2, 2, ResistorType("copper", 100))

def test_4x4_mesh():
	generator = Predefmesh()
	generator.grid(4, 4, ResistorType("copper", 100))

def test_8x8_mesh():
	generator = Predefmesh()
	generator.grid(8, 8, ResistorType("copper", 100))

def test_16x16_mesh():
	generator = Predefmesh()
	generator.grid(16, 16, ResistorType("copper", 100))

def test_32x32_mesh():
	generator = Predefmesh()
	generator.grid(32, 32, ResistorType("copper", 100))

def test_64x64_mesh():
	generator = Predefmesh()
	generator.grid(64, 64, ResistorType("copper", 100))

def test_128x128_mesh():
	generator = Predefmesh()
	generator.grid(128, 128, ResistorType("copper", 100))

def test_256x256_mesh():
	generator = Predefmesh()
	generator.grid(256, 256, ResistorType("copper", 100))

# def test_512x512_mesh():
#		generator = Predefmesh()
#		generator.grid(512, 512, ResistorType("copper", 100))
# 
# def test_1024x1024_mesh():
#		generator = Predefmesh()
#		generator.grid(1024, 1024, ResistorType("copper", 100))
