from lib.predefmesh import *
from lib.resistor_type import *
from lib.resistance_calculator import *

def test_4x4_mesh():
	generator = Predefmesh()
	mesh = generator.parallel_joined(4, 4, ResistorType("copper", 100))

	top_node = None
	bottom_node = None

	for node in mesh.nodes:
		if (top_node == None or top_node.point[0] < node.point[0]):
			top_node = node
		if (bottom_node == None or bottom_node.point[0] > node.point[0]):
			bottom_node = node

	ResistanceCalculator.equivalent_resistance((top_node, bottom_node), mesh)

def test_8x8_mesh():
	generator = Predefmesh()
	mesh = generator.parallel_joined(8, 8, ResistorType("copper", 100))

	top_node = None
	bottom_node = None

	for node in mesh.nodes:
		if (top_node == None or top_node.point[0] < node.point[0]):
			top_node = node
		if (bottom_node == None or bottom_node.point[0] > node.point[0]):
			bottom_node = node

	ResistanceCalculator.equivalent_resistance((top_node, bottom_node), mesh)

def test_16x16_mesh():
	generator = Predefmesh()
	mesh = generator.parallel_joined(16, 16, ResistorType("copper", 100))

	top_node = None
	bottom_node = None

	for node in mesh.nodes:
		if (top_node == None or top_node.point[0] < node.point[0]):
			top_node = node
		if (bottom_node == None or bottom_node.point[0] > node.point[0]):
			bottom_node = node

	ResistanceCalculator.equivalent_resistance((top_node, bottom_node), mesh)

def test_32x32_mesh():
	generator = Predefmesh()
	mesh = generator.parallel_joined(32, 32, ResistorType("copper", 100))

	top_node = None
	bottom_node = None

	for node in mesh.nodes:
		if (top_node == None or top_node.point[0] < node.point[0]):
			top_node = node
		if (bottom_node == None or bottom_node.point[0] > node.point[0]):
			bottom_node = node

	ResistanceCalculator.equivalent_resistance((top_node, bottom_node), mesh)

def test_64x64_mesh():
	generator = Predefmesh()
	mesh = generator.parallel_joined(64, 64, ResistorType("copper", 100))

	top_node = None
	bottom_node = None

	for node in mesh.nodes:
		if (top_node == None or top_node.point[0] < node.point[0]):
			top_node = node
		if (bottom_node == None or bottom_node.point[0] > node.point[0]):
			bottom_node = node

	ResistanceCalculator.equivalent_resistance((top_node, bottom_node), mesh)

def test_128x128_mesh():
	generator = Predefmesh()
	mesh = generator.parallel_joined(128, 128, ResistorType("copper", 100))

	top_node = None
	bottom_node = None

	for node in mesh.nodes:
		if (top_node == None or top_node.point[0] < node.point[0]):
			top_node = node
		if (bottom_node == None or bottom_node.point[0] > node.point[0]):
			bottom_node = node

	ResistanceCalculator.equivalent_resistance((top_node, bottom_node), mesh)
