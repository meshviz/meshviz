from lib.predefmesh import *
from lib.resistor_type import *
from lib.resistance_calculator import *

def test_4x4_mesh():
	generator = Predefmesh()
	mesh = generator.series_stacked(4, 4, ResistorType("copper", 100))

	top_node = None
	bottom_node = None

	for node in mesh.nodes:
		if (top_node == None or top_node.point[1] < node.point[1]):
			top_node = node
		if (bottom_node == None or bottom_node.point[1] > node.point[1]):
			bottom_node = node

	ResistanceCalculator.equivalent_resistance((top_node, bottom_node), mesh)

def test_8x8_mesh():
	generator = Predefmesh()
	mesh = generator.series_stacked(8, 8, ResistorType("copper", 100))

	top_node = None
	bottom_node = None

	for node in mesh.nodes:
		if (top_node == None or top_node.point[1] < node.point[1]):
			top_node = node
		if (bottom_node == None or bottom_node.point[1] > node.point[1]):
			bottom_node = node

	ResistanceCalculator.equivalent_resistance((top_node, bottom_node), mesh)

def test_16x16_mesh():
	generator = Predefmesh()
	mesh = generator.series_stacked(16, 16, ResistorType("copper", 100))

	top_node = None
	bottom_node = None

	for node in mesh.nodes:
		if (top_node == None or top_node.point[1] < node.point[1]):
			top_node = node
		if (bottom_node == None or bottom_node.point[1] > node.point[1]):
			bottom_node = node

	ResistanceCalculator.equivalent_resistance((top_node, bottom_node), mesh)

def test_32x32_mesh():
	generator = Predefmesh()
	mesh = generator.series_stacked(32, 32, ResistorType("copper", 100))

	top_node = None
	bottom_node = None

	for node in mesh.nodes:
		if (top_node == None or top_node.point[1] < node.point[1]):
			top_node = node
		if (bottom_node == None or bottom_node.point[1] > node.point[1]):
			bottom_node = node

	ResistanceCalculator.equivalent_resistance((top_node, bottom_node), mesh)

def test_64x64_mesh():
	generator = Predefmesh()
	mesh = generator.series_stacked(64, 64, ResistorType("copper", 100))

	top_node = None
	bottom_node = None

	for node in mesh.nodes:
		if (top_node == None or top_node.point[1] < node.point[1]):
			top_node = node
		if (bottom_node == None or bottom_node.point[1] > node.point[1]):
			bottom_node = node

	ResistanceCalculator.equivalent_resistance((top_node, bottom_node), mesh)

def test_128x128_mesh():
	generator = Predefmesh()
	mesh = generator.series_stacked(128, 128, ResistorType("copper", 100))

	top_node = None
	bottom_node = None

	for node in mesh.nodes:
		if (top_node == None or top_node.point[1] < node.point[1]):
			top_node = node
		if (bottom_node == None or bottom_node.point[1] > node.point[1]):
			bottom_node = node

	ResistanceCalculator.equivalent_resistance((top_node, bottom_node), mesh)

# def test_256x256_mesh():
#		generator = Predefmesh()
#		mesh = generator.series_stacked(256, 256, ResistorType("copper", 100))
# 
#		top_node = None
#		bottom_node = None
# 
#		for node in mesh.nodes:
#			if (top_node == None or top_node.point[1] < node.point[1]):
#				top_node = node
#			if (bottom_node == None or bottom_node.point[1] > node.point[1]):
#				bottom_node = node
# 
#		ResistanceCalculator.equivalent_resistance((top_node, bottom_node), mesh)

# def test_512x512_mesh():
#		generator = Predefmesh()
#		mesh = generator.series_stacked(512, 512, ResistorType("copper", 100))
# 
#		top_node = None
#		bottom_node = None
# 
#		for node in mesh.nodes:
#			if (top_node == None or top_node.point[1] < node.point[1]):
#				top_node = node
#			if (bottom_node == None or bottom_node.point[1] > node.point[1]):
#				bottom_node = node
# 
#		print(ResistanceCalculator.equivalent_resistance((top_node, bottom_node), mesh))

# def test_1024x1024_mesh():
#		generator = Predefmesh()
#		mesh = generator.series_stacked(1024, 1024, ResistorType("copper", 100))
# 
#		top_node = None
#		bottom_node = None
# 
#		for node in mesh.nodes:
#			if (top_node == None or top_node.point[1] < node.point[1]):
#				top_node = node
#			if (bottom_node == None or bottom_node.point[1] > node.point[1]):
#				bottom_node = node
# 
#		print(ResistanceCalculator.equivalent_resistance((top_node, bottom_node), mesh))
