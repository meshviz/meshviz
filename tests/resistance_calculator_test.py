# This tests the resistance calculator class.

from lib.mesh import *
from lib.resistor import *
from lib.resistance_calculator import *

import math

def test_simplify_series():
	node_0 = Node()
	node_1 = Node()
	node_2 = Node()
	node_3 = Node()
	node_4 = Node()
	length = 1
	resistors = [
			Resistor(ResistorType(None, 10), length, node_0, node_1),
			Resistor(ResistorType(None, 20), length, node_1, node_2),
			Resistor(ResistorType(None, 15), length, node_2, node_3),
			Resistor(ResistorType(None, 22), length, node_3, node_4),
	]

	resistance = ResistanceCalculator.series_resistance(resistors);
	assert resistance == 67

def test_simplify_parallel():
	node_0 = Node()
	node_1 = Node()
	node_2 = Node()
	node_3 = Node()
	length = 1

	resistors = [
			Resistor(ResistorType(None,10), length, node_0, node_1),
			Resistor(ResistorType(None,20), length, node_0, node_1),
			Resistor(ResistorType(None,15), length, node_0, node_1),
			Resistor(ResistorType(None,22), length, node_0, node_1),
	]

	resistance = ResistanceCalculator.parallel_resistance(resistors);
	assert resistance == 3.8150289017341037

def test_resistors_between_nodes():
	mesh = Mesh()

	node_0 = mesh.add_node()
	node_1 = mesh.add_node()
	node_2 = mesh.add_node()
	node_3 = mesh.add_node()
	length = 1

	resistors = [
			mesh.add_resistor(ResistorType(None, 10), length, node_1, node_2),
			mesh.add_resistor(ResistorType(None, 20), length, node_0, node_1),
			mesh.add_resistor(ResistorType(None, 15), length, node_0, node_2),
			mesh.add_resistor(ResistorType(None, 22), length, node_1, node_2),
	]

	between = ResistanceCalculator.resistors_between_nodes(node_0, node_1, mesh);
	assert len(between) == 1
	assert resistors[1] in between

	between = ResistanceCalculator.resistors_between_nodes(node_1, node_2, mesh);
	assert len(between) == 2
	assert resistors[0] in between
	assert resistors[3] in between

def test_resistors_between_nodes_backwards():
	node_0 = Node()
	node_1 = Node()
	node_2 = Node()
	node_3 = Node()
	length = 1

	mesh = Mesh()
	resistors = [
			mesh.add_resistor(ResistorType(None, 10), length, node_2, node_1),
			mesh.add_resistor(ResistorType(None, 20), length, node_1, node_0),
			mesh.add_resistor(ResistorType(None, 15), length, node_0, node_2),
			mesh.add_resistor(ResistorType(None, 22), length, node_1, node_2),
	]

	between = ResistanceCalculator.resistors_between_nodes(node_0, node_1, mesh);
	assert len(between) == 1
	assert resistors[1] in between

	between = ResistanceCalculator.resistors_between_nodes(node_1, node_2, mesh);
	assert len(between) == 2
	assert resistors[0] in between
	assert resistors[3] in between

def test_mesh_with_broken_resistor_in_series():
	mesh = Mesh()
	node_1 = mesh.add_node()
	node_2 = mesh.add_node()
	node_3 = mesh.add_node()
	length = 1

	mesh.add_resistor(ResistorType(None, 10), length, node_1, node_2)
	mesh.add_resistor(ResistorType(None, math.inf), length, node_2, node_3)

	equivalent_resistance = ResistanceCalculator.equivalent_resistance((node_1, node_3), mesh)

	assert equivalent_resistance == math.inf

def test_disconnected_nodes_returns_infinity():
	mesh = Mesh()
	node_1 = mesh.add_node()
	node_2 = mesh.add_node()
	node_3 = mesh.add_node()
	node_4 = mesh.add_node()
	length = 1

	mesh.add_resistor(ResistorType(None, 10), length, node_1, node_2)
	mesh.add_resistor(ResistorType(None, 10), length, node_3, node_4)

	equivalent_resistance = ResistanceCalculator.equivalent_resistance((node_1, node_3), mesh)

	assert equivalent_resistance == math.inf

def test_terminal_nodes_are_the_same_returns_0():
	mesh = Mesh()
	node_1 = mesh.add_node()
	node_2 = mesh.add_node()
	node_3 = mesh.add_node()
	node_4 = mesh.add_node()
	length = 1

	mesh.add_resistor(ResistorType(None, 10), length, node_1, node_2)
	mesh.add_resistor(ResistorType(None, 10), length, node_3, node_4)

	equivalent_resistance = ResistanceCalculator.equivalent_resistance((node_1, node_1), mesh)

	assert equivalent_resistance == 0
