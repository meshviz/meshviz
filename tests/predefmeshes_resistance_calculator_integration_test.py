from lib.predefmesh import *
from lib.resistance_calculator import *
from lib.resistor_type import *

def get_corner_nodes(mesh):
	corner_nodes = {}
	for node in mesh.nodes:
		above, below, left, right = False, False, False, False
		for resistor in node.resistors:
			if resistor.point.x > node.point.x:
				above = True
			if resistor.point.x < node.point.x:
				below = True
			if resistor.point.y < node.point.y:
				left = True
			if resistor.point.y > node.point.y:
				right = True
		if (above and left and not below and not right):
				corner_nodes["bottom_right"] = node
		if (not above and not left and below and right):
				corner_nodes["top_left"] = node
		if (above and not left and not below and right):
				corner_nodes["bottom_left"] = node
		if (not above and left and below and not right):
				corner_nodes["top_right"] = node
	return corner_nodes


def test_can_calculate_grid():
	predefmesh = Predefmesh()
	mesh = predefmesh.grid(3,3, ResistorType("copper", 100))
	corner_nodes = get_corner_nodes(mesh)
	assert int(ResistanceCalculator.equivalent_resistance((corner_nodes["top_left"], corner_nodes["bottom_right"]), mesh)) == 185
