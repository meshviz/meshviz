import sqlite3
from lib.clean import *
from lib.mesh import *
from lib.resistor_type import *
from lib.predefmesh import *

def test_series_stacked():
	clean = Clean()
	clean.clean_whole() # clean db to prepare for test

	copper_20 = ResistorType("copper", 20)
	length = 2
	predef = Predefmesh()
	predef.series_stacked(2, 3, copper_20).save()

	connection = sqlite3.connect("database.db")
	cursor = connection.cursor()

	cursor.execute('''SELECT * FROM meshes;''')
	meshes = cursor.fetchall()

	cursor.execute('''SELECT * FROM resistors; ''')
	resistors = cursor.fetchall()

	cursor.execute('''SELECT * FROM resistor_types; ''')
	resistor_types = cursor.fetchall()

	connection.commit()
	assert meshes == [(1,)]
	assert resistors.sort() == [(1,1,1, length, 0,1), (2,1,1, length, 1,3), (3,1,1, length, 3,5), (4,1,1, length, 0,2), (5,1,1, length, 2,4), (6,1,1, length, 4,5)].sort()
	assert resistor_types.sort() == [(1, "copper",20)].sort()

def test_parallel_joined():
	clean = Clean()
	clean.clean_whole() # clean db to prepare for test

	copper_20 = ResistorType("copper", 20)
	length = 2

	predef = Predefmesh()
	predef.parallel_joined(2, 3, copper_20).save()

	connection = sqlite3.connect("database.db")
	cursor = connection.cursor()

	cursor.execute('''SELECT * FROM meshes;''')
	meshes = cursor.fetchall()

	cursor.execute('''SELECT * FROM resistors; ''')
	resistors = cursor.fetchall()

	cursor.execute('''SELECT * FROM resistor_types; ''')
	resistor_types = cursor.fetchall()

	connection.commit()
	assert meshes == [(1,)]
	assert resistors.sort() == [(1,1,1, length, 0,1),(2,1,1, length, 0,1),(3,1,1, length, 0,1), (4,1,1, length, 1,2),(5,1,1, length, 1,2),(6,1,1, length, 1,2)].sort()
	assert resistor_types.sort() == [(1, "copper",20)].sort()

def test_grid():
	clean = Clean()
	clean.clean_whole() # clean db to prepare for test

	copper_20 = ResistorType("copper", 20)
	length = 2
	predef = Predefmesh()
	predef.grid(2, 2, copper_20).save()

	connection = sqlite3.connect("database.db")
	cursor = connection.cursor()

	cursor.execute('''SELECT * FROM meshes;''')
	meshes = cursor.fetchall()

	cursor.execute('''SELECT * FROM resistors; ''')
	resistors = cursor.fetchall()

	cursor.execute('''SELECT * FROM resistor_types; ''')
	resistor_types = cursor.fetchall()

	connection.commit()
	assert meshes == [(1,)]
	assert resistors.sort() == [(1,1,1, length, 1,4),(2,1,1, length, 4,7),(3,1,1, length, 2,5), (4,1,1, length, 5,8),(5,1,1, length, 3,6),(6,1,1, length, 6,9),(7,1,1, length, 1,2),(8,1,1, length, 2,3),(9,1,1, length, 4,5),(10,1,1, length, 5,6),(11,1,1, length, 7,8),(12,1,1, length, 8,9)].sort()
	assert resistor_types.sort() == [(1, "copper",20)].sort()

def test_grid_positions():
	copper_20 = ResistorType("copper", 20)

	predef = Predefmesh()
	mesh = predef.grid(2,1, copper_20)

	assert len(mesh.nodes) == 6
	assert len(mesh.resistors) == 7

	node_points = list(map(lambda node: (node.point.x, node.point.y),mesh.nodes))
	node_points.sort()

	expected_node_points = [
			(0,0), (0,100),
			(100, 0), (100, 100),
			(200, 0), (200, 100)
			]
	expected_node_points.sort()

	assert node_points == expected_node_points

	expected_resistor_points = [
			(0, 50),
			(50, 0), (50, 100),
			(100, 50),
			(150, 0), (150, 100),
			(200, 50)
			]
	expected_resistor_points.sort()

	resistor_points = list(map(lambda resistor: (resistor.point.x, resistor.point.y), mesh.resistors))
	resistor_points.sort()

	assert resistor_points == expected_resistor_points

def test_series_stacked_positions():
	copper_20 = ResistorType("copper", 20)

	predef = Predefmesh()
	mesh = predef.series_stacked(2, 3, copper_20)
	assert len(mesh.nodes) == 8
	assert len(mesh.resistors) == 9

	expected_node_points = [
			(1, 0),
			(0, 1), (1, 1), (2, 1),
			(0, 2), (1, 2), (2, 2),
			(1, 3),
			]
	expected_node_points = list(map(lambda point: (point[0] * 100, point[1] * 100), expected_node_points))
	expected_node_points.sort()

	node_points = list(map(lambda node: (node.point.x, node.point.y), mesh.nodes))
	node_points.sort()

	assert node_points == expected_node_points

	expected_resistor_points = [
			(0.5, 0.5), (1, 0.5), (1.5, 0.5),
			(0, 1.5), (1, 1.5), (2, 1.5),
			(0.5, 2.5), (1, 2.5), (1.5, 2.5),
			]
	expected_resistor_points = list(map(lambda point: (point[0] * 100, point[1] * 100), expected_resistor_points))
	expected_resistor_points.sort()

	resistor_points = list(map(lambda resistor: (resistor.point.x, resistor.point.y), mesh.resistors))
	resistor_points.sort()

	assert resistor_points == expected_resistor_points
