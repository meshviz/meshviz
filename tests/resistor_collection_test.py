import sqlite3
import pytest

from lib.clean import *
from lib.resistor_collection import *
from lib.resistor import *
from lib.point import *
from lib.node import *
from lib.resistor_type import *

def clean_database():
	Clean().clean_whole()

def test_saving_resistors_saves_points():
	clean_database()

	resistors = ResistorCollection()
	resistor_type = ResistorType("Copper", 20)

	node_1 = Node()
	node_2 = Node()
	node_1.id = 1
	node_2.id = 2

	res1 = Resistor(resistor_type, 1, node_1, node_2, (0, 50))
	res2 = Resistor(resistor_type, 1, node_1, node_2, (0, 100))
	resistors.add(res1)
	resistors.add(res2)

	resistors.save(12, "database.db")

	connection = sqlite3.connect("database.db")
	cursor = connection.cursor()

	cursor.execute("SELECT point_id FROM resistors")
	resistors = cursor.fetchall()

	assert resistors[0][0] > 0
	assert resistors[1][0] > 0
	assert resistors[1][0] != resistors[0][0]

	assert set(resistors) == set([(res1.point.id,), (res2.point.id,)])
