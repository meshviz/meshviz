import sqlite3

from lib.clean import *
from lib.mesh import *
from lib.resistor_type import *

def clean_database():
	Clean().clean_whole()

def test_clear_whole_database():
	clean_database()
	mesh = Mesh()
	mesh.save()

	copper_20 = ResistorType("copper", 20).save()
	copper_30 = ResistorType("copper", 30).save()
	copper_40 = ResistorType("copper", 40).save()
	copper_50 = ResistorType("copper", 50).save()
	length = 2

	node_0 = mesh.add_node()
	node_1 = mesh.add_node()
	node_2 = mesh.add_node()
	node_3 = mesh.add_node()
	node_4 = mesh.add_node()

	res1 = mesh.add_resistor(copper_20, length, node_0, node_1).save()
	res2 = mesh.add_resistor(copper_30, length, node_1, node_2).save()
	res3 = mesh.add_resistor(copper_40, length, node_2, node_3).save()
	res4 = mesh.add_resistor(copper_50, length, node_3, node_4).save()

	clean = Clean()
	clean.clean_whole()
	connection = sqlite3.connect("database.db")
	cursor = connection.cursor()

	cursor.execute('''SELECT * FROM meshes;''')
	meshes = cursor.fetchall()

	cursor.execute('''SELECT * FROM resistors; ''')
	resistors = cursor.fetchall()

	cursor.execute("SELECT id, resistivity, material FROM resistor_types;")
	resistor_types = cursor.fetchall()
	assert meshes == []
	assert resistors == []
	assert resistor_types == []

def test_if_database_full_with_valid_info():
	clean_database()

	mesh = Mesh()
	mesh.save()

	copper_20 = ResistorType("copper", 20).save()
	copper_30 = ResistorType("copper", 30).save()
	copper_40 = ResistorType("copper", 40).save()
	copper_50 = ResistorType("copper", 50).save()
	length = 2

	node_0 = mesh.add_node()
	node_1 = mesh.add_node()
	node_2 = mesh.add_node()
	node_3 = mesh.add_node()
	node_4 = mesh.add_node()

	res1 = mesh.add_resistor(copper_20, length, node_0, node_1).save()
	res2 = mesh.add_resistor(copper_30, length, node_1, node_2).save()
	res3 = mesh.add_resistor(copper_40, length, node_2, node_3).save()
	res4 = mesh.add_resistor(copper_50, length, node_3, node_4).save()

	connection = sqlite3.connect("database.db")
	cursor = connection.cursor()

	cursor.execute('''SELECT * FROM meshes;''')
	meshes = cursor.fetchall()

	cursor.execute('''SELECT id, mesh_id, resistor_type_id, length, from_node_id, to_node_id FROM resistors; ''')
	resistors = cursor.fetchall()

	cursor.execute("SELECT id, material, resistivity FROM resistor_types;")
	resistor_types = cursor.fetchall()

	connection.commit()
	assert meshes == [(mesh.id,)]
	assert set(resistors) == set([
			(res1.id, mesh.id, copper_20.id, length, node_0.id, node_1.id),
			(res2.id, mesh.id, copper_30.id, length, node_1.id, node_2.id),
			(res3.id, mesh.id, copper_40.id, length, node_2.id, node_3.id),
			(res4.id, mesh.id, copper_50.id, length, node_3.id, node_4.id)
			])
	assert resistor_types == [(1,"copper", 20), (2,"copper", 30),(3,"copper", 40),(4,"copper", 50)]

def test_save_entire_mesh():
	clean_database()

	mesh1 = Mesh()
	mesh2 = Mesh()

	mesh1.save() #used to check if it double saves mesh

	copper_70 = ResistorType("copper", 70)
	length = 2

	node_0 = mesh1.add_node()
	node_1 = mesh1.add_node()
	node_2 = mesh1.add_node()

	res1 = mesh1.add_resistor(copper_70, length, node_0, node_1)
	res2 = mesh1.add_resistor(copper_70, length, node_0, node_1)
	res3 = mesh2.add_resistor(copper_70, length, node_0, node_2)

	copper_70.save()

	res1.save()	#used to check if it double saves resistor

	mesh1.save()
	mesh2.save()

	connection = sqlite3.connect("database.db")
	cursor = connection.cursor()

	cursor.execute('''SELECT * FROM meshes;''')
	meshes = cursor.fetchall()

	cursor.execute("SELECT id, mesh_id, resistor_type_id, length, from_node_id, to_node_id FROM resistors")
	resistors = cursor.fetchall()

	cursor.execute("SELECT id, material, resistivity FROM resistor_types;")
	resistor_types = cursor.fetchall()

	connection.commit()
	assert meshes == [(mesh1.id,), (mesh2.id,)]
	assert resistors == [
			(res1.id, mesh1.id, copper_70.id, length, node_0.id, node_1.id),
			(res2.id, mesh1.id, copper_70.id, length, node_0.id, node_1.id),
			(res3.id, mesh2.id, copper_70.id, length, node_0.id, node_2.id)
			]
	assert resistor_types == [(1,"copper", 70)]


def test_save_resistor_and_new_resistor_type():
	clean_database()

	mesh1 = Mesh()

	copper_70 = ResistorType("copper", 70).save()
	bubble_gum_80 = ResistorType("bubblegum", 60).save()

	length_copper = 2
	length_bubblegum = 1

	node_0 = mesh1.add_node()
	node_1 = mesh1.add_node()

	res1 = mesh1.add_resistor(copper_70, length_copper, node_0, node_1)
	res2 = mesh1.add_resistor(bubble_gum_80, length_bubblegum, node_0, node_1)

	mesh1.save()

	connection = sqlite3.connect("database.db")
	cursor = connection.cursor()
	cursor.execute('''SELECT * FROM meshes;''')
	meshes = cursor.fetchall()

	cursor.execute("SELECT id, mesh_id, resistor_type_id, length, from_node_id, to_node_id FROM resistors")
	resistors = cursor.fetchall()

	cursor.execute("SELECT id, material, resistivity FROM resistor_types;")
	resistor_types = cursor.fetchall()

	connection.commit()
	assert meshes == [(mesh1.id,)]
	assert set(resistors) == set([(res1.id, mesh1.id, copper_70.id, length_copper, node_0.id, node_1.id), (res2.id, mesh1.id, bubble_gum_80.id, length_bubblegum, node_0.id, node_1.id)])
	assert resistor_types == [(1,"copper", 70), (2,"bubblegum", 60)]
