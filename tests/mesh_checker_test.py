import pytest

from lib.mesh import *
from lib.mesh_checker import *
from lib.resistor_type import *

def test_correct_mesh():
	mesh = Mesh()

	resistortype1 = ResistorType("copper", 50)
	resistortype2 = ResistorType("copper", 30)
	length = 1

	node_0 = mesh.add_node()
	node_1 = mesh.add_node()
	node_2 = mesh.add_node()
	node_3 = mesh.add_node()
	node_4 = mesh.add_node()

	mesh.add_resistor(resistortype1, length, node_0, node_1)
	mesh.add_resistor(resistortype2, length, node_1, node_2)
	mesh.add_resistor(resistortype1, length, node_2, node_3)
	mesh.add_resistor(resistortype2, length, node_3, node_4)

	result = MeshChecker.check(mesh)

	assert result == True

def test_incorrect_mesh():
	mesh = Mesh()

	resistortype1 = ResistorType("copper", 50)
	resistortype2 = ResistorType("copper", 30)
	length = 1

	node_0 = mesh.add_node()
	node_1 = mesh.add_node()
	node_2 = mesh.add_node()
	node_3 = mesh.add_node()
	node_4 = mesh.add_node()
	node_5 = mesh.add_node()
	node_6 = mesh.add_node()

	mesh.add_resistor(resistortype1, length, node_0, node_1)
	mesh.add_resistor(resistortype1, length, node_1, node_5)
	mesh.add_resistor(resistortype1, length, node_1, node_2)
	mesh.add_resistor(resistortype1, length, node_0, node_2)
	mesh.add_resistor(resistortype2, length, node_0, node_3)
	mesh.add_resistor(resistortype1, length, node_3, node_4)
	mesh.add_resistor(resistortype1, length, node_2, node_6)

	result = MeshChecker.check(mesh)

	assert	result == False

def test_check_specific_nodes_disconnected():
	mesh = Mesh()

	resistortype1 = ResistorType("copper", 50)
	resistortype2 = ResistorType("copper", 30)
	length = 1

	node_0 = mesh.add_node()
	node_1 = mesh.add_node()
	node_2 = mesh.add_node()
	node_3 = mesh.add_node()

	mesh.add_resistor(resistortype1, length, node_0, node_1)
	mesh.add_resistor(resistortype1, length, node_2, node_3)

	result = MeshChecker.check(mesh, set([node_0, node_2]))

	assert result == False

def test_check_specific_nodes_connected():
	mesh = Mesh()

	resistortype1 = ResistorType("copper", 50)
	resistortype2 = ResistorType("copper", 30)
	length = 1

	node_0 = mesh.add_node()
	node_1 = mesh.add_node()
	node_2 = mesh.add_node()
	node_3 = mesh.add_node()

	mesh.add_resistor(resistortype1, length, node_0, node_1)
	mesh.add_resistor(resistortype1, length, node_2, node_3)

	result = MeshChecker.check(mesh, set([node_0, node_1]))

	assert result == True
