from lib.mesh import *
from lib.resistor_type import *

def test_deleting_resistor_deletes_resistor_from_node():
	mesh = Mesh()
	node_0 = mesh.add_node()
	node_1 = mesh.add_node()
	resistor = mesh.add_resistor(ResistorType(None,0), 0, node_0, node_1)
	mesh.remove_resistor(resistor)
	assert node_0.resistors == set()
	assert node_1.resistors == set()

def test_deleting_last_resistor_of_node_deletes_node():
	mesh = Mesh()
	node_0 = mesh.add_node()
	node_1 = mesh.add_node()
	node_2 = mesh.add_node()
	deleted_resistor = mesh.add_resistor(ResistorType(None,0), 0, node_0, node_1)
	kept_resistor = mesh.add_resistor(ResistorType(None,0), 0, node_1, node_2)

	mesh.remove_resistor(deleted_resistor)
	assert mesh.nodes == set([node_1, node_2])
