import sqlite3
import pytest

from lib.clean import *
from lib.mesh import *
from lib.node import *
from lib.resistor import *
from lib.point import *

@pytest.fixture
def with_clean_db():
	cleaner = Clean()
	cleaner.clean_whole()
	yield
	cleaner.clean_whole()

@pytest.fixture
def saved_empty_mesh():
	return Mesh().save()

@pytest.fixture
def database_cursor():
	connection = sqlite3.connect("database.db")
	yield connection.cursor()
	connection.commit()

def test_do_not_save_empty_node(with_clean_db, saved_empty_mesh, database_cursor):
	point_x = 123
	point_y = 456
	node = Node(point = (point_x, point_y), mesh_id = saved_empty_mesh.id).save()

	database_cursor.execute('''
	SELECT nodes.id, nodes.mesh_id, points.x, points.y
	FROM nodes LEFT JOIN points ON nodes.point_id = points.id;
	''')
	nodes = database_cursor.fetchall()

	assert len(nodes) == 0

def test_can_save_node(with_clean_db, saved_empty_mesh, database_cursor):
	point_x = 123
	point_y = 456

	node = Node(point = (point_x, point_y), mesh_id = saved_empty_mesh.id)
	node.attach(Resistor(None, 10, node, Node()))
	node.save()

	database_cursor.execute('''
	SELECT nodes.id, nodes.mesh_id, points.x, points.y
	FROM nodes LEFT JOIN points ON nodes.point_id = points.id;
	''')
	nodes = database_cursor.fetchall()

	assert len(nodes) == 1
	assert node.id > 0
	assert nodes[0] == (node.id, saved_empty_mesh.id, point_x, point_y)
