# These are the integration tests for our mesh resistance value calculation.
import pytest

from lib.resistance_calculator import *
from lib.mesh import *
from lib.mesh_checker import *

db = DatabaseInterfacesManager()
from lib.resistor_type import *


def test_series_case():
	mesh = Mesh()
	copper_20 = ResistorType("copper", 20)
	copper_30 = ResistorType("copper", 30)
	copper_40 = ResistorType("copper", 40)
	copper_50 = ResistorType("copper", 50)

	length = 1
	node_0 = mesh.add_node()
	node_1 = mesh.add_node()
	node_2 = mesh.add_node()
	node_3 = mesh.add_node()
	node_4 = mesh.add_node()

	mesh.add_resistor(copper_20, length, node_0, node_1)
	mesh.add_resistor(copper_30, length, node_1, node_2)
	mesh.add_resistor(copper_40, length, node_2, node_3)
	mesh.add_resistor(copper_50, length, node_3, node_4)

	equivalent_resistance = ResistanceCalculator.equivalent_resistance((node_0, node_4), mesh)

	assert equivalent_resistance == 140

def test_parallel_case():
	mesh = Mesh()
	length = 1
	copper_20 = ResistorType("copper", 20)
	copper_30 = ResistorType("copper", 30)
	copper_40 = ResistorType("copper", 40)
	copper_50 = ResistorType("copper", 50)

	node_0 = mesh.add_node()
	node_1 = mesh.add_node()

	mesh.add_resistor(copper_20, length, node_0, node_1)
	mesh.add_resistor(copper_30, length, node_0, node_1)
	mesh.add_resistor(copper_40, length, node_0, node_1)
	mesh.add_resistor(copper_50, length, node_0, node_1)

	equivalent_resistance = ResistanceCalculator.equivalent_resistance((node_0, node_1), mesh)

	assert equivalent_resistance == pytest.approx(7.792, 0.001)

def test_resistor_with_star():
	mesh = Mesh()
	length = 1
	node_0 = mesh.add_node()
	node_1 = mesh.add_node()
	node_2 = mesh.add_node()
	node_3 = mesh.add_node()

	copper_20 = ResistorType("copper", 20)
	copper_30 = ResistorType("copper", 30)
	copper_40 = ResistorType("copper", 40)
	copper_50 = ResistorType("copper", 50)
	copper_60 = ResistorType("copper", 60)

	mesh.add_resistor(copper_50, length, node_0, node_1)
	mesh.add_resistor(copper_30, length, node_0, node_2)
	mesh.add_resistor(copper_40, length, node_1, node_2)
	mesh.add_resistor(copper_20, length, node_1, node_3)
	mesh.add_resistor(copper_60, length, node_2, node_3)

	equivalent_resistance = ResistanceCalculator.equivalent_resistance((node_0, node_3), mesh)

	assert equivalent_resistance == pytest.approx(36.5625, 0.001)

def test_resistor_with_delta():
	mesh = Mesh()
	copper_20 = ResistorType("copper", 20)
	copper_30 = ResistorType("copper", 30)
	copper_40 = ResistorType("copper", 40)
	copper_50 = ResistorType("copper", 50)
	copper_60 = ResistorType("copper", 60)
	length = 1

	node_0 = mesh.add_node()
	node_1 = mesh.add_node()
	node_2 = mesh.add_node()
	node_3 = mesh.add_node()
	node_4 = mesh.add_node()

	resistor_list = [
		mesh.add_resistor(copper_60, length, node_0, node_1),
		mesh.add_resistor(copper_50, length, node_1, node_2),
		mesh.add_resistor(copper_30, length, node_1, node_3),
		mesh.add_resistor(copper_40, length, node_2, node_3),
		mesh.add_resistor(copper_20, length, node_2, node_4),
		mesh.add_resistor(copper_60, length, node_3, node_4)
	]

	equivalent_resistance = ResistanceCalculator.equivalent_resistance((node_0, node_3), mesh)

	assert equivalent_resistance == pytest.approx(81.5625, 0.001)

def test_complex_case():
	mesh = Mesh()

	copper_50 = ResistorType("copper", 50)
	copper_30 = ResistorType("copper", 30)
	copper_40 = ResistorType("copper", 40)
	copper_20 = ResistorType("copper", 20)
	copper_60 = ResistorType("copper", 60)
	copper_70 = ResistorType("copper", 70)
	length = 1

	node_0 = mesh.add_node()
	node_1 = mesh.add_node()
	node_2 = mesh.add_node()
	node_3 = mesh.add_node()

	mesh.add_resistor(copper_50, length, node_0, node_1)
	mesh.add_resistor(copper_30, length, node_0, node_1)
	mesh.add_resistor(copper_40, length, node_0, node_1)
	mesh.add_resistor(copper_20, length, node_1, node_2)
	mesh.add_resistor(copper_60, length, node_0, node_2)
	mesh.add_resistor(copper_70, length, node_0, node_3)
	mesh.add_resistor(copper_60, length, node_3, node_1)

	equivalent_resistance = ResistanceCalculator.equivalent_resistance((node_0, node_2), mesh)

	assert equivalent_resistance == pytest.approx(20.709, 0.001)
