from lib.gui.window.main import *

def test_ask_before_exit(qtbot, monkeypatch):
	main_window = MainWindow()

	qtbot.addWidget(main_window)

	monkeypatch.setattr(QMessageBox, 'question', lambda *args: QMessageBox.No)

	main_window.close() == False

	monkeypatch.setattr(QMessageBox, 'question', lambda *args: QMessageBox.Yes)

	main_window.close() == True
