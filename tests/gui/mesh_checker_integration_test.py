from lib.gui.window.main import *
from lib.gui.window.mesh_check import *

from PyQt5.QtCore import Qt

def test_check_mesh(qtbot, monkeypatch):
	main_window = MainWindow()

	check_window = MeshCheckWindow(main_window)

	qtbot.addWidget(check_window)

	qtbot.waitForWindowShown(main_window)
	qtbot.waitForWindowShown(check_window)

	qtbot.mouseMove(check_window, check_window.check_button.pos())
	
	button = check_window.childAt(check_window.check_button.pos())
	assert button

	def error():
		raise Exception("Invalid response from mesh checker.");

	monkeypatch.setattr(QMessageBox, 'information', lambda *args: True )
	monkeypatch.setattr(QMessageBox, 'critical', lambda *args: error )

	qtbot.mouseClick(button, Qt.LeftButton, Qt.NoModifier, check_window.check_button.pos(), 100);

	monkeypatch.setattr(QMessageBox, 'question', lambda *args: QMessageBox.Yes)
	main_window.close() == True
