all: clean gui

clean:
	[ ! -f database.db ] || rm database.db
	[ ! -d build/ ] || rm -r build/
	[ ! -d dist/ ] || rm -r dist/

gui:
	python3 meshviz.py

run:
	./main.py

setup:
	pip3 install -r requirements.txt --user

show:
	sqlite3 database.db 'SELECT * FROM resistors;'

test: clean
	PYTHONPATH=. python3 -m pytest tests/ -s $(1) $(2) $(3)

package:
	pyinstaller --onefile --noconsole meshviz.py
